#include "defs.h"
#include "str_utils.h"

//genrates a new string, as the concatination of str1 and str2
char * str_cat_dup(const char * str1, const char * str2)
{
  char * str_cmb = NULL;
  
  str_cmb = malloc(sizeof(char) * (strlen(str1)+strlen(str2)+1));
  strcpy(str_cmb, str1);
  strcat(str_cmb, str2);
  return(str_cmb);
}

char * str_append(char ** dest, const char * src)
{ 
  *dest = realloc(*dest, sizeof(char) * (strlen(*dest) + strlen(src) + 1));
  strcat(*dest, src);
  
  return(*dest);
}

//compares a string to a prefix string
int str_prefix_cmp(const char * s_pre, int s_len, const char * str)
{
  int i;

  for (i = 0; i < s_len; ++i)
    {
      if (*(str + i) != *(s_pre + i))
	{
	  return(1);
	}
    }

  return(0);  
}

svect_t * svect_create()
{
  svect_t * svect = svect_alloc();

  *svect = (svect_t) { 0, 0, NULL };
  
  return svect;
}

int svect_append(svect_t * svect, const char * str)
{
  if (svect->count + 1 > svect->buffer)
    {
      svect->buffer += SVECT_BUFFER;
      svect->list = realloc(svect->list, (svect->buffer * sizeof(char *)));
      //initializes new list spaces to NULL
      for (int i = svect->count; i < svect->buffer; ++i)
	{
	  svect_item(svect, i) = NULL;
	}
    }

  (svect->count)++;
  *((svect->list) + (svect->count - 1)) = strdup(str);
  

  return 0;
}

int svect_nappend(svect_t * svect, const char * str, const size_t n)
{
  if (svect->count + 1 > svect->buffer)
    {
      svect->buffer += SVECT_BUFFER;
      svect->list = realloc(svect->list, (svect->buffer * sizeof(char *)));
      //initializes new list spaces to NULL
      for (int i = svect->count; i < svect->buffer; ++i)
	{
	  svect_item(svect, i) = NULL;
	}
    }

  (svect->count)++;
  *((svect->list) + (svect->count - 1)) = strndup(str, n);
  

  return 0;
}

int svect_remove(svect_t * svect, int num, int dist)
{
  int i;

  if (dist == 0)
    {
      return 0;
    }
  else if (dist < 0)
    {
      printf("distance less than zero\n");
      return 1;
    }
  else if ((num + dist) > svect->count) //may not catch one position overflow
    {
      printf("attempt to remove past list end\n");
      return 1;
    }

  for (i = num; i < (num + dist); ++i)
    {
      free(svect_item(svect, i));
      svect_item(svect, i) = NULL;
    }

  //loop for reconnecting items at the end if items are removed from the middle
  if ((num + dist) < (svect->count - 1))
    {
      char * hold = NULL;
      
      for (i = num; i + dist  < svect->count; ++i)
	{
	  hold = svect_item(svect, i + dist);
	  svect_item(svect, i + dist) = NULL;
	  svect_item(svect, i) = hold;
	}
    }

  svect->count -= dist;

  if (svect->count > 0)
    svect->buffer = ((svect->count / SVECT_BUFFER) + 1) * SVECT_BUFFER;
  else
    svect->buffer = 0;

  svect->list = realloc(svect->list, (svect->buffer * sizeof(char *)));

  return 0;
}
