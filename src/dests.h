#ifndef DESTS_H
#define DESTS_H
#include "defs.h"

typedef struct {
  int count;
  char **dests;
  char **destp;
} dest_t;

#define dest_list_alloc malloc(1 * sizeof(dest_t))

char **dest_dir_fmt(char **args, const int count);
char **destp_fmt(char **args, const int count);
int change_dest(dest_t *dest_list, int num);
void rm_dest(dest_t * dest_list);
int cat_str_list(char ***list1, const int list1_len, char **list2, const int list2_len);
char * check_for_dest_file();
int append_dests(dest_t *dest_list, char **new_entry, const int num);
dest_t fill_dests(const int args_num, char **args, char *dest_file);
void empty_dest_list(dest_t * dest_list);
void empty_dest_list_on_close(int status, void * arg);
int write_dests_to_file(const char *filename, const dest_t *dest_list);

#endif //DESTS_H
