#ifndef DISPLAY_H
#define DISPLAY_H
#include "defs.h"
#include <Imlib2.h>

typedef struct {
  Display * disp;
  Visual * vis;
  Colormap cm;
  int depth;
  //point_t mouse_pos;
  Imlib_Updates updates;
  Imlib_Font font;
  rect_t rect;
} x_disp_t;

typedef struct {
  Window screen;
  Imlib_Image buffer;
  Imlib_Font font;
  Imlib_Color_Range range;
} window_t;

extern x_disp_t display;

rect_t get_window_size(window_t * window);
void close_window(window_t * win);

#endif //DISPLAY_H
