#include "defs.h"
#include "undo.h"
#include "file_parsing.h"

extern status_t * curr_list_stat;

undo_list_t undo_stack;

ver_check_t ver_fix;

const static struct {
  int operation;
  int (*pop_func)(undo_list_t * stack);
} pop_func_list[] = {
  { NONE, NULL },
  { MOVE, pop_move },
  { COPY, pop_copy },
  { MULTI_START, pop_multi },
  { MULTI_STOP, pop_clean }
};

//initializes undo list
int undo_init()
{
  undo_stack.head = NULL;

  undo_stack.head = malloc(sizeof(undo_node));
  *(undo_stack.head) = (undo_node) { NONE, NULL, NULL, NULL };
  undo_stack.tail = undo_stack.head;
  
  return(0);
}

int undo_free_node(undo_node * node)
{
  if (node->data != NULL)
    {
      free(node->data->path);
      free(node->data->dest);
      free(node->data->newname);
      free(node->data);
    }
  free(node);

  return(0);
}

op_data * fill_data(char * path, file_entry * file, char * dest, char * newname)
{
  op_data * data = malloc(sizeof(op_data));

  *data = (op_data) { strdup(path),
		      file,
		      strdup(dest),
		      strdup(newname),
		      file->mtime };

  return(data);
}

int undo_eat_head(undo_list_t * stack)
{
  undo_node * rem = stack->head;
  if (stack->head->prev != NULL)
    {
      stack->head = stack->head->prev;
      stack->head->next = NULL;
    }
  else
    stack->head = NULL;

  stack->total--;
  
  undo_free_node(rem);

  return(0);
}

int undo_eat_tail(undo_list_t * stack)
{
  undo_node * new_tail = stack->tail->next->next;
  new_tail->prev = NULL;
  undo_free_node(stack->tail->next);
  stack->tail->next = new_tail;
  
  return(0);
}

//create new undo node at head of the undo list
int undo_append(undo_list_t * stack, int undo_type, op_data * data)
{
  if (stack->total > undomax)
    {
      undo_eat_tail(stack);
    }
  else
    (stack->total)++;
  undo_node *old = stack->head;
  stack->head = malloc(sizeof(undo_node));
  *(stack->head) = (undo_node) { undo_type, data, NULL, NULL };
  stack->head->prev = old;
  if (old != NULL)
    old->next = stack->head;
  else
    stack->tail = stack->head;

  return(0);
}

void undo_push_r(undo_list_t * stack, int undo_type, char * path, file_entry * file, char * dest, char * newname)
{
  op_data * data = NULL;
  
  //fill_data(path, file, dest, newname);
  if (undo_type == MULTI_START || undo_type == MULTI_STOP)
    {
      data = NULL;
    }
  else
    {
      data = fill_data(path, file, dest, newname);
    }
  undo_append(stack, undo_type, data);
  
  return;
}

void undo_push(int undo_type, char * path, file_entry * file, char * dest, char * newname)
{
  undo_push_r(&undo_stack, undo_type, path, file, dest, newname);
  
  return;
}

int undo_pop_r(undo_list_t * stack)
{
  uint i, hold = 0;

  if (stack->head == NULL)
      return(1);

  for (i = 0; i < ARRAYSIZE(pop_func_list); ++i)
    {
      if (stack->head->operation == pop_func_list[i].operation)
	{
	  if (pop_func_list[i].pop_func == NULL)
	    {
	      return(1);
	    }
	  hold = pop_func_list[i].pop_func(stack);
	  break;
	}
    }

  if (hold == 0)
    undo_eat_head(stack);
  
  return(0);
}

void undo_pop()
{
  undo_pop_r(&undo_stack);
  
  return;
}

int pop_move(undo_list_t * stack)
{
  undo_node * top = stack->head;

  move_name(top->data->dest,
	    top->data->newname,
	    top->data->path,
	    top->data->file->d_name);
  
  top->data->file->mtime = top->data->mtime;
  curr_list_stat->rem--;

  printf("undid moving of file: %s\n", top->data->file->d_name);

  return(0);
}

int pop_copy(undo_list_t * stack)
{
  char * filepath = NULL;
  op_data * data = stack->head->data;
  
  filepath = path_cat(data->dest, data->newname);
    
  if (unlink(filepath) != 0)
    printf("unable to delete file: %s\n", data->newname);
  else
    printf("undid copying of file: %s\n", data->file->d_name);
  
  free(filepath);
  
  return(0);
}

int pop_multi(undo_list_t * stack)
{
  int done = 0, hold = 0;

  undo_eat_head(stack);

  while (!done)
    {
      if (stack->head == NULL)
	{
	  done = 1;
	  hold = 1;
	}
      else if (stack->head->operation == MULTI_STOP)
	{
	  done = 1;
	}
      else
	{
	  done = undo_pop_r(stack);
	  hold = 1;
	}
    }
  
  return(hold);
}

int pop_clean(undo_list_t * stack)
{
  printf("errent end of multi_reading undo_node found\n");

  undo_eat_head(stack);

  return(0);
}

void undo_empty_list(undo_list_t * stack)
{
  while (stack->head != NULL)
    {
      undo_eat_head(stack);
    }

  return;
}

void undo_empty_list_on_close()
{
  undo_empty_list(&undo_stack);

  return;
}
