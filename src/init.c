#include "defs.h"
#include "init.h"
#include "display.h"

//x_disp_t display;

void init()
{
  display.disp = XOpenDisplay(NULL);
  if (display.disp == NULL)
    {
      printf("Could not open X display\n");
      exit(1);
    }

  display.vis = DefaultVisual(display.disp, DefaultScreen(display.disp));
  display.depth = DefaultDepth(display.disp, DefaultScreen(display.disp));
  display.cm = DefaultColormap(display.disp, DefaultScreen(display.disp));

  // imlib2 initialization
  imlib_set_cache_size(imgcache * 1024 * 1024);
  imlib_set_font_cache_size(fontcache * 1024 * 1024);
  imlib_set_color_usage(128);
  imlib_context_set_dither(1);
  imlib_context_set_anti_alias(1);
  imlib_context_set_display(display.disp);
  imlib_context_set_visual(display.vis);
  imlib_context_set_colormap(display.cm);

  imlib_add_path_to_font_path(default_font_loc);
  display.font = imlib_load_font(default_font);

  //display.mouse_x = 0;
  //display.mouse_y = 0;

  display.rect.x = 0;
  display.rect.y = 0;
  display.rect.w = WidthOfScreen(DefaultScreenOfDisplay(display.disp));
  display.rect.h = HeightOfScreen(DefaultScreenOfDisplay(display.disp));
  
  return;
}


void cleanup(void)
{
  imlib_context_set_font(display.font);
  imlib_free_font();
  XCloseDisplay(display.disp);

  return;
}

