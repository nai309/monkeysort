#ifndef SETTINGS_H
#define SETTINGS_H
#include "defs.h"
#include "controls.h"

struct tag_conv {
  int tag;
  char * tag_str;
};

typedef struct category {
  char * category_str;
  int (*handler)(char **token_list, int tag);
  int (*writer)(FILE * stream, struct category * cat);
  int tag_len;
  struct tag_conv * tags;
} category_t;

enum {
  DEF_DEST_FILE_NAME_O,
  DELIM_O,
  DEFAULT_FONT_LOC_O,
  DEFAULT_FONT_O,
  //  DESTMAX_O,
  HISTMAX_O,
  UNDOMAX_O,
  IMGCACHE_O,
  FONTCACHE_O,
#ifdef VID
  VIDEO_PLAYER_O,
#endif
  ZOOM_CONSTANT_O,
  NULL_O
};

enum {
  SORT_MODE_FL,
  MOVE_BEHAVIOR_FL,
  QUIET_FL,
  NONUM_FL,
  VERBOUS_FL,
  NO_DEST_FL,
  PULL_UP_TEXT_FL,
  NULL_FL
};

FILE *open_conf(char *config_path);
int str_to_tag(const char *tag_string, category_t * category);
int line_tok(char *conf_line, category_t * category);
category_t * type_string_conv(const char * tag_string);
int select_conf(char * conf_line);
char * trim_comments(const char * line);
int conf_set(char *conf_path);
void add_mod(const char * mod_str, int * mods);
char * set_mods(char * key_str, int * mods);
char * get_mods(const int mods);
int set_control(char **token_list, int tag);
int write_controls(FILE * stream, struct category * cat);
int set_setting(char **token_list, int tag);
int write_settings(FILE * stream, struct category * cat);
int set_option(char **token_list, int tag);
int write_options(FILE * stream, struct category * cat);
int generate_default_conf(char *conf_path);
int list_conf();

#endif //SETTINGS_H
