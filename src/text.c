#include "defs.h"
#include "text.h"
#include "img.h"
#include "history.h"
#include "file_io.h"
#include "dest_window.h"
#include "controls.h"

extern unsigned int dest_keys[DESTMAX];

extern vect_list_t * dest_list_g;

window_t text_win;

rect_t text_rect, def_rect, tab_rect;

//creates text input window
void text_create_window(const char *title)
{
  if (flag & Q_TEXT)
    text_win.screen = img_window.window.screen;
  else
    text_win.screen = XCreateSimpleWindow(display.disp, DefaultRootWindow(display.disp),
					  0, 0,
					  460, 100,
					  0, 0, 0);

  text_win.font = display.font;
  
  text_rect = get_window_size(&text_win);

  def_rect = text_rect;

  if (flag & Q_TEXT)
    return;
  
  XSelectInput(display.disp, text_win.screen,
	       ButtonPressMask | ButtonReleaseMask |  KeyPressMask |
	       ExposureMask | StructureNotifyMask);

  XMapRaised(display.disp, text_win.screen);

  XChangeProperty(display.disp, text_win.screen,
		  XInternAtom(display.disp, "_NET_WM_NAME", False),
		  XInternAtom(display.disp, "UTF8_STRING", False),
		  8, PropModeReplace, (unsigned char *) title,
		  strlen(title));

  Atom atoms[1] = { XInternAtom(display.disp, "WM_DELETE_WINDOW", 0) };
  
  XSetWMProtocols(display.disp, text_win.screen, atoms, 1);
    
  return;
}

int render_text(Imlib_Image buffer)
{
  imlib_context_set_drawable(text_win.screen);
  imlib_context_set_blend(0);
  imlib_context_set_image(buffer);
  if (flag & Q_TEXT)
    {
      int text_height = imlib_image_get_height();
      imlib_render_image_on_drawable(0, text_rect.h - text_height);
    }
  else
    imlib_render_image_on_drawable(0, 0);
  imlib_free_image();

  return(0);
}

svect_t * gen_message_str(const char * text)
{
  int w = 0, wt = 0, line_used = 0, hold = 0, line_len = strlen(text);
  char hold_text[2048], hold_char_str[2] = { '\0', '\0'};
  svect_t * message = svect_create();

  imlib_context_set_font(text_win.font);

  while (line_used < line_len)
    {
      hold = 0;
      wt = 0;
      do
	{
	  hold_char_str[0] = *(text + line_used + hold);
	  imlib_get_text_size(hold_char_str, &w, NULL);
	  wt += w;

	  if (wt < (text_rect.w - 10))
	    {
	      hold_text[hold] = *(text + line_used + hold);
	      hold++;
	    }
	}
      while ((wt < (text_rect.w - 10)) &&
	     ((line_used + hold) < line_len));
      hold_text[hold] = '\0';
      
      svect_append(message, hold_text);
      line_used += hold;
    }
  
  return(message);  
}

svect_t * gen_tab_str(svect_t * dir_list, const char * text)
{
  int list_used = 0, w = 0, wt = 0;
  char  * line = NULL,
    delim_str[] = {' ', delim, ' ', '\0'};
  svect_t  * message = gen_message_str(text);

  imlib_context_set_font(text_win.font);

  while (list_used < dir_list->count)
    {
      line = strdup(*(dir_list->list + list_used));
      list_used++;
      imlib_get_text_size(line, &w, NULL);
      wt = w;
      while ((wt < (text_rect.w - 10)) && (list_used < dir_list->count))
	{
	  imlib_get_text_size(delim_str, &w, NULL);
	  wt += w;
	  imlib_get_text_size(*(dir_list->list + list_used), &w, NULL);
	  wt += w;
	  
	  if (wt < (text_rect.w - 10))
	    {
	      STR_APPEND(line, delim_str);
	      STR_APPEND(line, *(dir_list->list + list_used));
	      list_used++;
	    }
	}
      svect_append(message, line);
      free(line);
    }
  
  return(message);
}

rect_t get_message_rect(const Imlib_Font font, const svect_t * message)
{
  rect_t ret_rect = {0, 0, 0, 0};
  int i, vertical = 0, width = 0;

  imlib_context_set_font(font);
  
  for (i = 0; i < message->count; ++i)
    {
      imlib_get_text_size(svect_item(message, i), &width, &vertical);

      ret_rect.h += vertical;
      if (width > ret_rect.w)
	ret_rect.w = width;
    }
  
  return(ret_rect);
}

Imlib_Image redrw(svect_t * message)
{
  int i, height = 0, vertical = 0;
  Imlib_Image buffer;
 
  text_rect = get_window_size(&text_win);
  
  tab_rect = get_message_rect(display.font, message);

  if (!(flag & Q_TEXT))
    XResizeWindow(display.disp, text_win.screen,
		  text_rect.w,
		  TEXT_MIN_HEIGHT(tab_rect.h, def_rect.h));

  text_rect = get_window_size(&text_win);
  if (flag & Q_TEXT)
    buffer = imlib_create_image(text_rect.w, tab_rect.h + 10);
  else
    buffer = imlib_create_image(text_rect.w, text_rect.h);
  imlib_context_set_blend(1);
  render_background(&buffer, 0, 0, 0);

  imlib_context_set_color(255, 255, 255, 255);
  imlib_context_set_image(buffer);
  imlib_context_set_font(display.font);
  
  height = 0;
  for (i = 0; i < message->count; ++i)
    {
      imlib_text_draw_with_return_metrics(0, height,
					  svect_item(message, i),
					  NULL, NULL, NULL, &vertical);
      height += vertical;
    }

  svect_empty(message);
  free(message);
  message = NULL;
  
  return(buffer);
}

int split_dir_frag(char * text, char ** dir, char ** complete)
{
  int dirlen = 0;
  char * name = strrchr(text, '/');
  
  if (name == NULL)
    {
      *dir = strdup("");
      *complete = strdup(text);
      return(1);
    }
  else
    {
      dirlen = name - text;
      *dir = malloc(sizeof(char) * (dirlen + 2));
      *dir = strncpy(*dir, text, dirlen + 1);
      *(*dir + dirlen + 1) = '\0';
      *complete = strdup((text + dirlen + 1));
      return(0);
    }
}

int completion(char ** text, svect_t * dir_list, char ** prefix_out)
{
  svect_t dir_candidates = {0, 0, NULL}, full_list = {0, 0, NULL};
  char * dir_path = NULL, * prefix = NULL;
  int no_dir = 0, ret_val = 0;

  no_dir = split_dir_frag((*text + 1), &dir_path, &prefix);

  if (no_dir == 1)
    {
      char * cdir = getcwd(NULL, 0);
      fill_dir_list_r(&full_list, cdir);
      free(cdir);
    }
  else
    {
      fill_dir_list_r(&full_list, dir_path);
    }

  dir_compare_r(&full_list, &dir_candidates, prefix);

  if (dir_candidates.count == 1)
    {
      free(*text);
      *text = strdup(">");
      STR_APPEND(*text, dir_path);
      STR_APPEND(*text, *(dir_candidates.list));
      check_dir(text);
      svect_empty(&dir_candidates);
      ret_val = 0;
    }
  else if (dir_candidates.count > 1)
    {
      uint i;
      int j, pre_len = strlen(prefix);
      for (i = pre_len; i < strlen(*(dir_candidates.list)); ++i)
	{
	  char hold_str[2] = { '\0', '\0' };
	  hold_str[0] = *(*(dir_candidates.list) + i);
	  if (hold_str[0] == '\0')
	    {
	      goto done;
	    }
	  for (j = 1; j < dir_candidates.count; ++j)
	    {
	      if (*(*(dir_candidates.list + j) + i) != hold_str[0])
		{
		  goto done;
		}
	    }
	  STR_APPEND(prefix, hold_str);
	  STR_APPEND(*text, hold_str);
	}
    done:
      *dir_list = dir_candidates;
      ret_val = 1;
    }

  free(dir_path);
  if (*prefix_out != NULL) free(*prefix_out);
  *prefix_out = strdup(prefix);
  free(prefix);
  svect_empty(&full_list);
  
  return(ret_val);
}

//function for text input loop
char *text_in (char *title, int hist_n)
{
  int done = 0;
  XEvent event;
  KeySym current_key;
  mod_t mod;
  char * text = NULL, * outtext = NULL, current_char[16],
    hold_text[2048], segment[1024], * prefix = NULL;
  int char_buff_len = 16;
  int tab_index = -1;
  int old_h = 0;
  svect_t dir_list = {0, 0, NULL};
  Imlib_Image buffer;
  
  int length = 0, redraw = 0;
  int comp_state = 0;
  
  text = strdup(">"); //mallocs and initializes text string
  
  text_create_window(title);
  
  text_rect.x = 0;  
  text_rect.y = 0;
  
  buffer = redrw(gen_message_str(text));
  render_text(buffer);
  
  while (!done) {
    
    do
      {
	XNextEvent(display.disp, &event);
	
	if (event.type == Expose)
	  {
	    text_rect = get_window_size(&text_win);
	    redraw = 1;
	    update_render = 1;
	  }
	
	if (event.type ==  DestroyNotify)
	  {
	    if (event.xdestroywindow.window == text_win.screen)
	      {
	    done = 1;
	    text = realloc(text, sizeof(char) * 3);
	    text = strcpy(text, ">\n");
	    hist_trav(0, NULL, (hists + hist_n));
	      }
	  }
	
	else if ( event.type == KeyPress )
	  {
	    mod = convert_mod(event.xkey.state);
	    
	    XLookupString(&event.xkey, current_char,
			  char_buff_len, &current_key, NULL);
	    
	    if( current_key == XK_BackSpace )
	      {
		length = strlen(text) - 1;
		if (length > 0)
		  {
		    *(text + length) = '\0';
		    if (comp_state == 1)
		      {
			svect_empty(&dir_list);
			if (prefix != NULL) P_FREE(prefix);
			tab_index = -1;
			comp_state = 0;
		      }
		    redraw = 1;
		  }
	      }
	    else if ( current_key == XK_Up )
	      {
		hist_trav(UP, &text, (hists + hist_n));
		redraw = 1;
	      }
	    else if ( current_key == XK_Down )
	      {
		hist_trav(DOWN, &text, (hists + hist_n));
		redraw = 1;
	      }
	    else if ( mod & MOD_CTRL )
	      {
		if ( current_key == TEXT_CANCEL_KEY )
		  {
		    done = 1;
		    text = realloc(text, sizeof(char) * 3);
		    text = strcpy(text, ">\n");
		    hist_trav(0, NULL, (hists + hist_n));
		  }
		
		else if (comp_state == 1)
		  {
		    for (int i = 10; i < ((dir_list).count + 10); ++i)
		      {
			if (current_key == (dest_keys[i]))
			  {
			    char * name = strrchr(text, '/');
			    if (name == NULL)
			      name = text + 1;
			    else
			      name = name + 1;
			    *name = '\0';
			    STR_APPEND(text, *(((dir_list).list) + (i-10)));
			    redraw = 1;
			    break;
			  }
		      }
		  }
	      }
	    else if ( current_key == XK_Tab || current_key == XK_ISO_Left_Tab) //tab completion
	      {
		if (hist_n == DEST_HIST)
		  {
		    if (comp_state != 1)
		      {
			comp_state = completion(&text, &dir_list, &prefix);
			strcpy(hold_text, text);
		      }
		    else if (comp_state == 1)
		      {
			if (mod & MOD_SHIFT)
			  {
			    if (tab_index > 0)
			      {
				tab_index--;
			      }
			    else
			      tab_index = dir_list.count - 1;
			  }
			else
			  {
			    if (tab_index < dir_list.count - 1)
			      {
				tab_index++;
			      }
			    else
			      tab_index = 0;
			  }
			free(text);
			strcpy(segment,
			       svect_item(&dir_list, tab_index) + strlen(prefix));
			text = str_cat_dup(hold_text, segment);
		      }
		    redraw = 1;
		  }
	      }
	    else if ( current_key == XK_Return  || current_key == XK_KP_Enter )
	      {
		done = 1;
	      }
	    else if (ISLATINKEY(current_key))
	      {
		STR_APPEND(text, current_char);
		XFlush(display.disp);
		if (comp_state == 1)
		  {
		    svect_empty(&dir_list);
		    if (prefix != NULL) P_FREE(prefix);
		    tab_index = -1;
		    comp_state = 0;
		  }
		redraw = 1;
	      }
	  }
      }
    while (XPending(display.disp));
    if (redraw == 1)
      {
	if (comp_state == 0)
	  buffer = redrw(gen_message_str(text));
	else if (comp_state == 1)
	  buffer = redrw(gen_tab_str(&dir_list, text));
       
	if ((flag & Q_TEXT))
	  {
	    if (tab_rect.h < old_h)
	      {
		update_render = 1;
	      }
	    old_h = tab_rect.h;
	  }
      }
    if (update_render == 1)
      {
	imlib_context_set_drawable(img_window.window.screen);
	loading_img(img_window.path);
	if (dest_window_state == 1)
	  {
	    dest_window_redrw((dest_list_current(dest_list_g)));
	  }
	render_img();
	update_render = 0;
      }
    if (redraw == 1)
      {
	render_text(buffer);
	redraw = 0;
      }
    
    
    usleep(16000);
  }
  
  if (dir_list.list != NULL) svect_empty(&dir_list);
  if (prefix != NULL) free(prefix);
  
  if (*(text + 1) == '\0')
    {
      text = realloc(text, sizeof(char) * 3);
      text = strcpy(text, ">\n");
    }
    
  if (*(text + 1) != '\n')
    {
      hist_set(text, (hists + hist_n));
    }
  
  outtext = strdup((text + 1));
  free(text);

  if (!(flag & Q_TEXT))
    close_window(&text_win);

  update_render = 1; //might want to move outside of this function
  
  return (outtext);
}
