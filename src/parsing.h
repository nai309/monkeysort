#ifndef PARSING_H
#define PARSING_H
#include "defs.h"
#include "file_parsing.h"

int get_num();
int get_dest(char **dest_path);
int new_dir();
int move(file_list_t * file_list, const int pos, char *dest);
int re_name(char *path, file_entry *file, char *dest);
int rm_file(file_list_t * file_list, int pos);
int seek(file_list_t * file_list);
int multi_reading(file_list_t * file_list, char * dest_path, int num, const int direction);
void index_scroll(const int num);
int scroll_reading(file_list_t * file_list, char * dest_path);

#endif //PARSING_H
