#pragma GCC diagnostic ignored "-Wswitch"
#include "defs.h"
#include "input_cont.h"
#include "img.h"
#include "parsing.h"
#include "dest_window.h"
#include "misc.h"
#include "history.h"
#include "text.h"
#include "file_io.h"
#include "undo.h"

#define FILE_LIST (file_list_current(arg_list.file_vect_list))
#define FILE_VECT (arg_list.file_vect_list)
#define DEST_LIST (dest_list_current(arg_list.dest_vect_list))
#define DEST_VECT (arg_list.dest_vect_list)

extern status_t * curr_list_stat;

int cycle_cont(input_arg_list_t arg_list, exts_t ext)
{
  if (scroll.on == 1)
    index_scroll(ext);
  next_file_vect_img(FILE_VECT, ext);

  return 0;
}

//container function for changing destinations
int change_dest_cont(input_arg_list_t arg_list, exts_t ext)
{
  int hold;

  if (ext == CHANGE)
    {
      hold = get_num();
      if (hold + NUM_CHECK > DESTMAX)
	printf("out of range\n");
      else if (hold < 0)
	printf("number less than one\n");
      else if (hold != 0)
	change_dest(DEST_LIST, (hold + NUM_CHECK));
    }
  else if (ext == ADD)
    {
      if ( DEST_LIST->count + NUM_CHECK >= DESTMAX )
	printf("max number of destinations reached\n");
      else
	{
	  //DEST_LIST->count++;
	  hold = change_dest(DEST_LIST, (DEST_LIST->count + NUM_CHECK + 1));
	  //if (hold == 1)
	  //DEST_LIST->count--;
	}
    }
  else if (ext == REMOVE)
    {
      rm_dest(DEST_LIST);
    }
  
  if (dest_window_state == 1)
    {
      update_render = 1;
    }
  
  return(0);
}

int print_cont(input_arg_list_t arg_list, exts_t ext)
{
  switch (ext)
    {
    case LIST:
      list_dests(DEST_LIST);
      break;
    case LINE:
      dest_line(DEST_LIST);
      break;
    case PRINT:
      printf("%s\n", CURR_FILE_NAME(FILE_LIST));
      break;
    case DF_PRINT:
      {
	char * text = text_in("enter file for destinations", NULL_HIST);
	if (text != NULL && *text != '\n')
	  {
	    write_dests_to_file(text, DEST_LIST);
	  }
	free(text);
      }
      break;
    default:
      break;
    }

  return(0);
}

int exit_cont(input_arg_list_t arg_list, exts_t ext)
{
  exit(0);

  return(0);
}

int seek_cont(input_arg_list_t arg_list, exts_t ext)
{
  int position = seek(FILE_LIST);
  if(position != 0)
    {
      next_file_vect_img(FILE_VECT, position);
    }
      
  return(0);
}

int new_dir_cont(input_arg_list_t arg_list, exts_t ext)
{
  new_dir();

  return(0);
}

int scroll_cont(input_arg_list_t arg_list, exts_t ext)
{
  if (scroll.on == 0)
    {
      scroll.on = 1;
      scroll.num = 1;
      scroll.pos = FILE_LIST->list_stat.pos;
      img_set_title(CURR_FILE_NAME(FILE_LIST));
    }
  else if (scroll.on == 1)
    {
      scroll.on = 0;
      scroll.num = 1;
      img_set_title(CURR_FILE_NAME(FILE_LIST));
    }
  
  return(0);
}

int mod_mode_cont(input_arg_list_t arg_list, exts_t ext)
{
  switch (ext)
    {
    case MOD_RENAME:
      if (mod_mode == 'r')
	mod_mode = '0';
      else
	mod_mode = 'r';
      break;
    case MOD_COPY:
      if (mod_mode == 'c')
	mod_mode = '0';
      else
	mod_mode = 'c';
      break;
    case MOD_COPY_HOLD:
      if (mod_mode == 'C')
	mod_mode = '0';
      else
	mod_mode = 'C';
      break;
    case MOD_OVERWRITE:
      if (flag & OVER)
	{
	  flag &= ~OVER;
	}
      else
	{
	  flag = (flag | OVER) & ~RENAME;
	}
      break;
    case MOD_VERSION:
      if (flag & RENAME)
	{
	  flag &= ~RENAME;
	}
      else
	{
	  flag = (flag | RENAME) & ~OVER;
	}
      break;
    case MOD_EXTEN:
      if (flag & EXTEN)
	{
	  flag &= ~EXTEN;
	}
      else
	{
	  flag |= EXTEN;
	}
      break;
    }

  img_set_title(CURR_FILE_NAME(FILE_LIST));
  
  return(0);
}

int undo_cont(input_arg_list_t arg_list, exts_t ext)
{
  undo_pop();

  return(0);
}

int dest_window_cont(input_arg_list_t arg_list, exts_t ext)
{
  dest_window_toggle();
  
  return(0);
}

int zoom_cont(input_arg_list_t arg_list, exts_t ext)
{
  zoom_image(ext);

  return(0);
}

int move_cont(input_arg_list_t arg_list, exts_t ext)
{
  int hold = 1;
  char * dest_name = NULL;
  
  switch (ext) //can probably be improved and extra redundancy removed
    {
    case SINGLE:
      if (scroll.on == 1)
	{
	  if (get_dest(&dest_name) == 0)
	    {
	      hold = scroll_reading(FILE_LIST, dest_name);
	      free(dest_name);
	    }
	  scroll.num = 1;
	  scroll.on = 0;
	}
      else
	{
	  if (get_dest(&dest_name) == 0)
	    {
	      hold = multi_reading(FILE_LIST, dest_name, 1, DOWN);
	      free(dest_name);
	    }
	}
      break;
    case MULTI:
      hold = get_num();
      if (hold == 0) break;
      if (get_dest(&dest_name) == 0)
	{
	  hold = multi_reading(FILE_LIST, dest_name, hold, DOWN);
	  free(dest_name);
	}
      break;
    case REVMULTI:
      hold = get_num();
      if (hold == 0) break;
      if (get_dest(&dest_name) == 0)
	{
	  hold = multi_reading(FILE_LIST, dest_name, hold, UP);
	  free(dest_name);
	}
      break;
    case RE_NAME:
      hold = re_name(FILE_LIST->path, CURR_FILE(FILE_LIST), FILE_LIST->path);
      if (hold == 0)
	{
	  hold = 1;
	}
      break;
    case DELETE:
      hold = rm_file(FILE_LIST, (FILE_LIST->list_stat.pos));
      break;
    }

  if (hold != 1)
    next_file_vect_img(FILE_VECT, DOWN);
  else
    img_set_title(CURR_FILE_NAME(FILE_LIST));

  return(0);
}

int dest_cont(input_arg_list_t arg_list, int key)
{
  int i;

  for ( i=NUM_CHECK; i < DEST_LIST->count + NUM_CHECK; ++i )
    {
      if ((unsigned int) key == (dest_keys[i]))
	{
	  if (scroll.on == 1)
	    {
	      scroll_reading(FILE_LIST, *(DEST_LIST->dests + i));
	      scroll.num = 1;
	      scroll.on = 0;
	      next_file_vect_img(FILE_VECT, DOWN);
	    }
	  else
	    {
	      if (move(FILE_LIST, (FILE_LIST->list_stat.pos), *(DEST_LIST->dests + i)) == 0)
		{
		  next_file_vect_img(FILE_VECT, DOWN);
		}
	      else
		img_set_title(CURR_FILE_NAME(FILE_LIST));
	    }
	}
    }
  
  return(0);
}

int change_file_vect_list(input_arg_list_t arg_list, exts_t ext)
{
  if (scroll.on != 1)
    {
      int hold = vect_list_index(FILE_VECT, ext);
      
      if (hold == 0)
	{
	  curr_list_stat = &((FILE_LIST)->list_stat);
	  next_file_vect_img(FILE_VECT, 0);
	  update_render = 1;
	}
    }
  
  return(0);
}

int change_dest_vect_list(input_arg_list_t arg_list, exts_t ext)
{
  int hold = vect_list_index(DEST_VECT, ext);

  if (hold == 0)
    {
      if (dest_window_state == 1)
	{
	  update_render = 1;
	}
    }
  
  return(0);
}

#ifdef VID

char * video_player = VIDEO_PLAYER_DEF;

int play_video(input_arg_list_t arg_list, exts_t ext)
{
  if (on_vid == 0)
    {
      printf("not a video file\n");
      return(0);
    }
  char * filepath = path_cat(FILE_LIST->path, CURR_FILE_NAME(FILE_LIST));
  char * filepath_quoted = str_cat_dup(" \'", filepath);
  free(filepath);
  filepath_quoted = STR_APPEND_R(filepath_quoted, "\'");
  char * command_string = str_cat_dup(video_player, filepath_quoted);
  free(filepath_quoted);
  printf("%s\n", command_string);

  if (system(command_string) == -1)
    {
      printf("video player failed to launch\n");
      free(command_string);
      return(1);
    }

  free(command_string);
  return(0);
}

#endif
