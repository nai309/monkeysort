#include "defs.h"
#include "img.h"
#include "vect_wrapper.h"

#define ZOOM_DIFF(length) ((((int) (length * zoom_factor)) - length) / 2)

img_window_t img_window;

double zoom_factor = 1;

#ifdef VID
int on_vid;
#endif

extern status_t * curr_list_stat;

void img_create_window(char * title)
{
  
  img_window.window.screen = XCreateSimpleWindow(display.disp,
						 DefaultRootWindow(display.disp),
						 0, 0,
						 1024, 768,
						  0, 0, 0);

  XSelectInput(display.disp, img_window.window.screen, ButtonPressMask | ButtonReleaseMask | 
	       KeyPressMask | PointerMotionMask | ExposureMask | StructureNotifyMask);

  XMapWindow(display.disp, img_window.window.screen);

  XChangeProperty(display.disp, img_window.window.screen,
		  XInternAtom(display.disp, "_NET_WM_NAME", False),
		  XInternAtom(display.disp, "UTF8_STRING", False),
		  8, PropModeReplace, (unsigned char *) title,
		  strlen(title));
  
  img_window.path = NULL;
  
  return;
}

void img_destroy_window()
{
  close_window(&(img_window.window));
  
  return;
}

//can leak if not freed outside of function
//creates title string for use in the img_set_title function
char *titling(const char *file)
{
  char *title = NULL;
  int length = 0, ind = 0;

  if (scroll.on == 1)
    {
      length += snprintf(NULL, 0, "scroll: %i ", scroll.num);
    }
    if (curr_list_stat->rem > 0)
    {
      length += snprintf(NULL, 0, "[%i - %i(-%i)] - ", curr_list_stat->pos+1, curr_list_stat->len, curr_list_stat->rem);
    }
  else
    {
  length += snprintf(NULL, 0, "[%i - %i] - ", curr_list_stat->pos+1, curr_list_stat->len);
    }

  title = malloc(sizeof(char)*(length + 1));
  if (scroll.on == 1)
    {
      ind += sprintf((title+ind), "scroll: %i ", scroll.num);
    }
  if (curr_list_stat->rem > 0)
    {
      ind += sprintf((title+ind), "[%i - %i(-%i)] - ", curr_list_stat->pos+1, curr_list_stat->len, curr_list_stat->rem);
    }
  else
    {
      ind += sprintf((title+ind), "[%i - %i] - ", curr_list_stat->pos+1, curr_list_stat->len);
    }
  
  if (mod_mode != '0')
    {
      char * hold = NULL;

      hold = title;
      title = malloc(strlen(hold) + 5);
      sprintf(title, "%c - ", mod_mode);
      strcat(title, hold);
      free(hold);
    }
  
  STR_APPEND(title, file);
  return(title);
}

int img_set_title(const char * file)
{
  char * title = NULL;
  
  title = titling(file);

  XChangeProperty(display.disp, img_window.window.screen,
		  XInternAtom(display.disp, "_NET_WM_NAME", False),
		  XInternAtom(display.disp, "UTF8_STRING", False),
		  8, PropModeReplace, (unsigned char *) title,
		  strlen(title));

  free(title);

  return(0);
}

int render_background(Imlib_Image * buffer, const int r, const int g, const int b)
{
  imlib_context_set_image(*buffer);
  imlib_context_set_color(r, g, b, 255);
  imlib_image_fill_rectangle(0, 0, imlib_image_get_width(), imlib_image_get_height());
  
  return(0);
}

rect_t scale_image(const rect_t src_rect, const rect_t dest_rect)
{
  rect_t scale_rect;
  double ratio = (((double) src_rect.w) / ((double) src_rect.h) /
		  ((double) dest_rect.w / (double) dest_rect.h));
  
  if (ratio > 1.0)
    {
      scale_rect.x = 0;
      scale_rect.w = dest_rect.w;
      scale_rect.h = (int) ((double) dest_rect.h / ratio);
      scale_rect.y = (int) ((double) dest_rect.h / 2.0) - ((double) scale_rect.h / 2.0);
    }
  else
    {
      scale_rect.y = 0;
      scale_rect.w = (int) ((double) dest_rect.w * ratio);
      scale_rect.h = dest_rect.h;
      scale_rect.x = (int) ((double) dest_rect.w / 2.0) - ((double) scale_rect.w / 2.0);
    }
  
  return(scale_rect);
}

void zoom_image(const int direction)
{
  if (direction > 0)
    {
      zoom_factor *= zoom_constant;
    }
  else if (direction < 0)
    {
      if (zoom_factor > 0.01)
	zoom_factor /= zoom_constant;
    }

  update_render = 1;
  
  return;
}

int loading_img(const char *path)
{
  Imlib_Image image;
  image = imlib_load_image(path);
  if(!image)
    {
      return(2);
    }
  XWindowAttributes atts;
  XGetWindowAttributes(display.disp, img_window.window.screen, &atts);
  rect_t image_rect = {0, 0, 0, 0},
    win_rect = {0, 0, atts.width, atts.height},
    scale_rect;
  img_window.img_buffer = imlib_create_image(atts.width, atts.height);
  imlib_context_set_image(img_window.img_buffer);
  imlib_context_set_blend(1);
  render_background(&(img_window.img_buffer), 47, 47, 47);
  
  imlib_context_set_image(image);
  image_rect.w = imlib_image_get_width();
  image_rect.h = imlib_image_get_height();

  scale_rect = scale_image(image_rect, win_rect);

  imlib_context_set_image(img_window.img_buffer);

  imlib_blend_image_onto_image(image, 0,
			       0, 0, image_rect.w, image_rect.h,
			       0 + scale_rect.x + mouse_delta.x - ZOOM_DIFF(scale_rect.w), 0 + scale_rect.y + mouse_delta.y - ZOOM_DIFF(scale_rect.h),
			       (int) (scale_rect.w * zoom_factor), (int) (scale_rect.h * zoom_factor));

  imlib_context_set_image(image);
  imlib_free_image();

  imlib_context_set_image(img_window.img_buffer);
  
  return(0);
}

//attempts to load image, returning 0 if successful, and 2 if unsuccessful
int test_img(const char * path)
{
  Imlib_Image image;
  image = imlib_load_image(path);
  if(!image)
    {
      return(2);
    }
  imlib_context_set_image(image);
  imlib_free_image();

  return(0);
}

int render_img()
{
  imlib_context_set_blend(0);
  imlib_context_set_drawable(img_window.window.screen);
  imlib_context_set_image(img_window.img_buffer);
  imlib_render_image_on_drawable(0, 0);

  imlib_free_image();
  
  return(0);
}
