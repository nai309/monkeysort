#include "defs.h"
#include "main.h"
#include "init.h"
#include "input.h"
#include "file_parsing.h"
#include "str_utils.h"
#include "parsing.h"
#include "dests.h"
#include "img.h"
#include "settings.h"
#include "history.h"
#include "undo.h"
#include "misc.h"
#include "dest_window.h"
#include "directory_completion.h"
#include "vect_wrapper.h"

uint8_t flag = 0;

scroll_t scroll;

char mod_mode;

status_t * curr_list_stat;

vect_list_t * rollover_file_vect_list;

vect_list_t * dest_list_g;

int time = 1;

static struct option long_opt_list[] = {
  {"overwrite", no_argument, NULL, 'o'},
  {"alphabetic", no_argument, NULL, 'a'},
  {"recent", no_argument, NULL, 'u'},
  {"inputdir", required_argument, NULL, 'i'},
  {"start", required_argument, NULL, 's'},
  {"rename", no_argument, NULL, 'r'},
  {"extention", no_argument, NULL, 'e'},
  {"quiet", no_argument, NULL, 'q'},
  {"nonum", no_argument, NULL, 'n'},
  {"verbose", no_argument, NULL, 'v'},
  {"destfile", required_argument, NULL, 'd'},
  {"help", no_argument, NULL, 'h'},
  {"config", required_argument, NULL, 'c'},
  {"filter", required_argument, NULL, 'f'},
  {"generate-config", required_argument, NULL, GEN_CONF},
  {"no-list-winow", no_argument, NULL, 'l'},
  {"no-default-destfile", no_argument, NULL, NO_DEST},
  {"working-directory", required_argument, NULL, 'w'},
  {"slide-up-text", no_argument, NULL, 't'}
};

void init_exits(vect_list_t * file_vect_list, vect_list_t * dest_vect_list);
char * arg_config(int argc, char * const argv[]);
int check_for_user_dirs();

int main(int argc, char **argv)
{
  int go = 1, start = 1, option = 0, infl = 0, long_ind = 0;
  char *conf_path = NULL,
    *dir_path = NULL;
  svect_t * fvect = svect_create();
  svect_t * dvect = svect_create();

  vect_list_t * file_vect_list = vect_list_init_m(file_list_decon);
  rollover_file_vect_list = file_vect_list;
  vect_list_t * dest_vect_list = vect_list_init_m(dest_list_decon);
  dest_list_g = dest_vect_list;
  
  control_sizes();
  hist_init();
  undo_init();
  //display.updates = imlib_updates_init(); //imlib updates
  
  if (argc < 2) 
    {
      printf("usage: " PRGM_NAME " [OPTIONS] SRC_DIR [DEST_DIR(1-%i)]\n", DESTMAX);
      exit(1);
    }

  conf_path = arg_config(argc, argv);

  if (check_for_user_dirs() != 0)
    {
      printf("exiting\n");
      exit(1);
    }
  
  if (!conf_path)
    {
      char * def_conf_path = path_cat(getenv("HOME"), USER_CONF_LOCATION);
      if (access(def_conf_path, F_OK) != -1)
	{
	  conf_set(def_conf_path);
	}
      else if (access(DEF_CONF_LOCATION, F_OK) != -1)
	{
	  conf_set(DEF_CONF_LOCATION);
	}
      free(def_conf_path);
    }
  else
    conf_set(conf_path);
  
  while ((option = getopt_long(argc, argv, "oaui:s:rqnhec:d:vldw:tf:", long_opt_list, &long_ind)) != -1)
    {
      switch (option)
	{
	case 'o': //enable file overwrite
	  flag = (flag | OVER) & ~RENAME;
	  printf("warning, use of overwrite mode may lead to data loss\n");
	  break;
	  
	case 'a': //sort alphabetically
	  time = 0;
	  break;

	case 'u': //sort from newest to oldest
	  time = 2;
	  break;

	case 'i': //source directory flag
	  svect_append(fvect, optarg);
	  infl = 1;
	  break;

	case 's': //start at position
	  start = strtol(optarg, NULL, 0);
	  break;

	case 'r': //move and version name mode
	  flag = (flag | RENAME) & ~OVER;
	  break;

	case 'e': //changeable extensions mode
	  flag |= EXTEN;
	  break;

	case 'q': //quiet mode
	  flag |= QUIET;
	  break;

	case 'n': // use only the top row number keys
	  flag |= NONUM;
	  break;

	case 'v':
	  flag |= VERBOUS;
	  break;

	case 'd': //pull destinations from file
	  svect_append(dvect, optarg);
	  break;

	case 'h':
	  //help_msg();
	  printf("help is currently not available\n");
	  exit(0);
	  break;

	case 'c':
	  //conf_path = optarg;
	  break;

	case 'f':
	  filter = strdup(optarg);
	  break;

	case GEN_CONF:
	  generate_default_conf(optarg);
	  printf("config generated\n");
	  exit(0);
	  break;

	case 'l':
	  dest_window_state = 0;
	  break;

	case NO_DEST:
	  flag |= USE_DEST;
	  break;

	case 'w':
	  dir_path = optarg;
	  break;
	case 't':
	  flag |= Q_TEXT;
	  break;
	  
	default:
	  break;
	}
    }
  if (infl == 0)
    {
      if (argc <= optind)
	{
	  printf("no source folder specified\n");
	  exit(1);
	}
      svect_append(fvect, *(argv + optind));
    }
  
  optind = optind - infl; //adjustment for use with i flag

  if (dir_path != NULL)
    {
      chdir(dir_path);
    }

  dest_vect_list_fill(dest_vect_list, argc, argv, dvect);

  if (!(flag & VERBOUS))
    {
      stderr = fopen("/dev/null", "w"); //removes libpng malformed warnings
    }
 
  if (dest_list_current(dest_vect_list)->count > DESTMAX - NUM_CHECK)
    {
      printf("too many destinations\n");
      exit(1);
    }
   
  init();

  for (int i = 0; i < fvect->count; ++i)
    {
      vect_list_append(file_vect_list, (void *) file_list_alloc);
      fill_file_list(svect_item(fvect, i), (file_list_t *) vect_list_item(file_vect_list, i), start, time);
      start = 1; //so -s flag only applies to the first file_list
    }
  svect_empty(fvect);
  free(fvect);

  init_exits((file_vect_list), (dest_vect_list));

  //sets source for list position for titling function
  curr_list_stat = &((file_list_current(file_vect_list))->list_stat);

  img_create_window("Images");
      
  next_file_vect_img(file_vect_list, 0);
  
  while (go == 1)
    {
      get_input((file_vect_list), (dest_vect_list));

      
      usleep(16000);
    }
  
  
  exit(0);
}

void init_exits(vect_list_t * file_vect_list, vect_list_t * dest_vect_list)
{
  atexit(cleanup);
  atexit(img_destroy_window);

  on_exit(remove_vect_list_on_close, (void *) (file_vect_list));
  on_exit(remove_vect_list_on_close, (void *) (dest_vect_list));
  atexit(undo_empty_list_on_close);
  atexit(hist_empty_on_close);
  on_exit(print_vfcount, (void *) (file_vect_list));

  return;
}

//function for checking for config option, before parsing all other options
char * arg_config(int argc, char * const argv[])
{
  char * path = NULL;
  int i;

  for (i = 0; i < argc; ++i)
    {
      if (!(strcmp(argv[i], "-c")) || !(strcmp(argv[i], "--config")))
	{
	  if (i < (argc - 1))
	    {
	      path = argv[i + 1];
	    }
	}
    }
  
  
  return path;  
}

int check_for_user_dirs()
{
  int success = 0;
  char * config_dir = path_cat(getenv("HOME"), USER_DIR_LOCATION);
#ifdef VID
  char * cache_dir = path_cat(getenv("HOME"), CACHE_DIR_LOCATION);
#endif

  if (access(config_dir, F_OK) != 0)
    {
      success = mkdir(config_dir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    }
  free(config_dir);
  if (success != 0)
    {
      printf("could not create user config directory\n");
      #ifdef VID
      free(cache_dir);
      #endif
      return success;
    }
  #ifdef VID
  if (access(cache_dir, F_OK) != 0)
    {
      success = mkdir(cache_dir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    }
  free(cache_dir);
  if (success != 0)
    {
      printf("could not create preview cache directory\n");
      return success;
    }
  #endif
  
  return success;
}
