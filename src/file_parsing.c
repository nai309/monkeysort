#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include "defs.h"
#include <fcntl.h>
#include <sys/sendfile.h>
#include <fnmatch.h>
#include "file_parsing.h"
#include "str_utils.h"

extern ver_check_t ver_fix;

char * filter = NULL;

// indexes the filelist in the given direction
void index_dir(file_list_t * file_list, const int direction)
{
  file_list->list_stat.pos += direction;
  
  if (file_list->list_stat.pos > (file_list->list_stat.len - 1) ||
      file_list->list_stat.pos < 0)
    {
      exit(0);
    }
  
  while (CURR_FILE(file_list)->mtime == 1)
    {
      if (direction == 0)
	file_list->list_stat.pos += 1;
      else
	file_list->list_stat.pos += direction;
      if (file_list->list_stat.pos > (file_list->list_stat.len - 1) ||
        file_list->list_stat.pos < 0)
	{
	  exit(0);
	}
    }
  
  return;
}

// modifies path string
char *check_dir(char **path) //adds trailing slash
{
  int len;

  len = (strlen(*path));
  if (*(*path + len - 1) != '/')
    {
      *path = realloc(*path, sizeof(char) * (len + 2));
      *(*path + len) = '/';
      *(*path + len + 1) = '\0';
    }

  return(*path);
}

// allocates new string
// must be free'd outside of function
char *check_dir_m(const char *path) //adds trailing slash
{
  int len;
  char *fpath = NULL;

  len = (strlen(path));
  if (*(path + len - 1) != '/')
    {
      fpath = malloc(sizeof(char) * (len + 2));
      fpath = strcpy(fpath, path);
      *(fpath + len) = '/';
      *(fpath + len + 1) = '\0';
    }
  else
    {
      fpath = strdup(path);
    }
  
  return(fpath);
}

//concatinates the path and filename
//must be free'd outside of function
char *path_cat(const char *path, const char *file)
{
  char * filepath = NULL;

  if (*(path + strlen(path) - 1) != '/')
    {
      filepath = check_dir_m(path);
    }
  else
    {
      filepath = strdup(path);
    }
  
  STR_APPEND(filepath, file);
  return(filepath);
}

// generates a new filename by appending the given ver number to the oldname
char *ver_name(const char *oldname, const int ver)
{
  char *newname = NULL, *ver_string = NULL;
  int oldname_len, ext_len, rootname_len, ver_len;

  oldname_len = strlen(oldname);
  ext_len = strlen(strchr(oldname, '.'));
  rootname_len = oldname_len - ext_len;
  ver_string = malloc(snprintf(NULL, 0, " (%i)", ver) + 1);
  ver_len = sprintf(ver_string, " (%i)", ver);
  newname = malloc(oldname_len + ver_len + 1);
  newname = strncpy(newname, oldname, rootname_len);
  *(newname + rootname_len) = '\0';
  newname = strcat(newname, ver_string);
  newname = strcat(newname, (oldname + rootname_len));
  free(ver_string);

  return(newname);
}

// returns the extension from the given filename, or the null byte of the filename if no extension exists
char *ext_rename(char *name)
{
  char *extname = NULL;
  int name_len, ext_len, rootname_len;

  name_len = strlen(name);
  ext_len = strlen(strchrnul(name, '.'));
  rootname_len = name_len - ext_len;
  extname = (name + rootname_len);

  return(extname);
}

// core file copy function
int copy_name(char *path, char *filename, char *dest, char *newname)
{
  char *old_path = NULL, *new_path = NULL, *newvername = NULL;
  int ver = 1, src_fd, dest_fd, copy_size;
  struct stat src_file;
  off_t src_offset = 0;
  
  if (access(dest, F_OK) < 0) { printf("destination doesn't exist\n"); return 1; }
  old_path = path_cat(path, filename);
  new_path = path_cat(dest, newname);
  if (access(new_path, F_OK) != -1 && (flag & RENAME))
    {
      printf("file: %s exists, renaming\n", filename);
      while (access(new_path, F_OK) != -1)
	{
	  if (newvername != NULL)
	    free(newvername);
	  newvername = ver_name(filename, ver);
	  free(new_path);
	  new_path = path_cat(dest, newvername);
	  //free(newvername);
	  ver++;
	}
      ver_fix.used = 1;
      ver_fix.name = newvername;
    }
  else if (access(new_path, F_OK) != -1 && !(flag & OVER))
    {
      printf("file: %s already exists\n", filename);
      free(old_path); free(new_path);
      return 1;
    }
  if ((src_fd = open(old_path, O_RDONLY)) == -1)
    {
      printf("source file: %s does not exist\n", filename);
      free(old_path); free(new_path);
      return 1;
    } 
  if ((dest_fd = creat(new_path, 0660)) == -1)
    {
      printf("file: %s already exists\n", filename);
      close(src_fd);
      free(old_path); free(new_path);
      return 1;
    }
  free(old_path); free(new_path);
  fstat(src_fd, &src_file);
  if ((copy_size = sendfile(dest_fd, src_fd, &src_offset, src_file.st_size)) < 0)
    {
      printf("could not copy file: %s\n", filename);
      close(src_fd); close(dest_fd);
      return 1;
    }
  else if (copy_size != src_file.st_size) //may need to add call to remove in this case
    {
      printf("could not copy file: %s\n", filename);
      close(src_fd); close(dest_fd);
      return 1;
    }
  else
    {
      printf("file: %s copied\n", filename);
      close(src_fd); close(dest_fd);
      return 0;
    }
}

//does not have many safety checks
int move_across_device(char *old_path, char *new_path)
{
  int src_fd, dest_fd, copy_size;
  struct stat src_file;
  off_t src_offset = 0;
  if ((src_fd = open(old_path, O_RDONLY)) == -1)
    {
      return 1;
    } 
  if ((dest_fd = creat(new_path, 0660)) == -1)
    {
      close(src_fd);
      return 1;
    }
  fstat(src_fd, &src_file);
  if ((copy_size = sendfile(dest_fd, src_fd, &src_offset, src_file.st_size)) < 0)
    {
      close(src_fd); close(dest_fd);
      return 1;
    }
  else if (copy_size != src_file.st_size) 
    {
      close(src_fd); close(dest_fd);
      return 1;
    }

  struct timespec times[2] = { src_file.st_atim, src_file.st_mtim };
  futimens(dest_fd, times);
  fchmod(dest_fd, src_file.st_mode);

  close(src_fd); close(dest_fd);
  if (unlink(old_path) != 0)
    {
      printf("unable to delete file at: %s\n", old_path);
    }
  
  return 0;
}

//core file moving function, wrapped by other functions for specific purposes
int move_name(char *path, char *filename, char *dest, char *newname)
{
  char *old_path = NULL, *new_path = NULL, *newvername = NULL;
  int ver = 1;
  
  if (access(dest, F_OK) < 0)
    {
      printf("destination: %s doesn't exist\n", dest);
      return 1;
    }
  old_path = path_cat(path, filename);
  new_path = path_cat(dest, newname);
  if (access(new_path, F_OK) != -1 && flag & RENAME)
    {
      printf("file exists, renaming\n");
      while (access(new_path, F_OK) != -1)
	{
	  if (newvername != NULL)
	    free(newvername);
	  newvername = ver_name(filename, ver);
	  free(new_path);
	  new_path = path_cat(dest, newvername);
	  //free(newvername);
	  ver++;
	}
      ver_fix.used = 1;
      ver_fix.name = newvername;
    }
  else if (access(new_path, F_OK) != -1 && !(flag & OVER))
    {
      printf("file: %s already exists\n", filename);
      free(old_path);
      free(new_path);
      return 1;
    }
  if (rename(old_path, new_path) < 0)
    {
      if (errno == EXDEV)
	{
	  if (move_across_device(old_path, new_path) > 0)
	    {
	      printf("could not move file: %s\n", filename);
	      free(old_path); free(new_path);
	      return 1;
	    }
	  free(old_path); free(new_path);
	  return 0;
	}
      printf("could not move file: %s\n", filename);
      free(old_path); free(new_path);
      return 1;
    }
  else
    {
      free(old_path); free(new_path);
      return 0;
    }
}

// converts a dirent list to a filelist struct
void file_list_conv(file_list_t * file_list, struct dirent **dirlist)
{
  int i, j, len = file_list->list_stat.len;
  struct stat buf;
  char *filepath = NULL;

  for (i = 0, j = 0; i < len; ++i)
    {
      filepath = path_cat(file_list->path, (*(dirlist + i))->d_name);
      stat(filepath, &buf);
      free(filepath);
      if (buf.st_mode & S_IFDIR) //trims directories out of filelist
	{ 
	  (file_list->list_stat.len)--;
	  continue;
	}
      (*(file_list->namelist + j)) = malloc(sizeof(file_entry));
      (*(file_list->namelist + j))->d_name = strdup((*(dirlist + i))->d_name);
      (*(file_list->namelist + j))->mtime = buf.st_mtime;
      ++j;
    }
  
  return;
}
  
//consider using qsort_r in order to make these one function

//file_entry modification time qsort function, from oldest to newest
int time_comp(const void *a, const void *b)
{
  file_entry *fileA = *(file_entry **)a;
  file_entry *fileB = *(file_entry **)b;
  
  return(fileA->mtime - fileB->mtime);
}

//file_entry modification time qsort function, from newest to oldest
int time_comp_up(const void *a, const void *b)
{
  file_entry *fileA = *(file_entry **)a;
  file_entry *fileB = *(file_entry **)b;
  
  return(fileB->mtime - fileA->mtime);
}

//wrapper function for fnmatch so that it can be passed to scandir
int filter_check(const struct dirent * dir)
{
  int success = fnmatch(filter, dir->d_name, 0);

  if (success == 0)
    return 1;
  else
    return 0;
}

// function for creating a file_list from a specified source path
int fill_file_list(const char * src_path, file_list_t * file_list, const int start, const int time)
{
  int dirent_len = 0;
  struct dirent ** dirent_list;
  
  file_list->path = realpath(src_path, NULL);
  
  if (file_list->path == NULL) 
    {
      printf("source does not exist\n");
      exit(1);
    }
  
  file_list->path = check_dir(&(file_list->path));
  
  file_list->list_stat.rem = 0;
  if (filter == NULL)
    file_list->list_stat.len = scandir(file_list->path, &dirent_list, NULL, versionsort);
  else
    file_list->list_stat.len = scandir(file_list->path, &dirent_list, filter_check, versionsort);
  file_list->list_stat.pos = start - 1;
  
  file_list->namelist = (file_entry **)malloc(file_list->list_stat.len * sizeof(file_entry *));
  
  dirent_len = file_list->list_stat.len;
  file_list_conv(file_list, dirent_list);
  
  for (int i = 0; i < dirent_len; ++i)
    {
      free(*(dirent_list + i));
    }
  free(dirent_list);
  
  if (file_list->list_stat.len == 0)
    {
      printf("directory was either empty, or there were no results based on the given filter, exiting.\n");
      exit(1);
    }
  
  
  if (time == 1)
    qsort(file_list->namelist,
	  (file_list->list_stat.len),
	  sizeof(file_entry *),
	  time_comp);
  else if (time == 2)
    qsort(file_list->namelist,
	  (file_list->list_stat.len),
	  sizeof(file_entry *),
	  time_comp_up);
  
  return(0);
}

// empties the given file_list
void empty_file_list(file_list_t * file_list)
{
  int i;

  for (i = 0; i < file_list->list_stat.len; ++i)
    {
      free((*(file_list->namelist + i))->d_name);
      (*(file_list->namelist + i))->d_name = NULL;
      free(*(file_list->namelist + i));
      *(file_list->namelist + i) = NULL;
    }
  free(file_list->namelist);
  file_list->namelist = NULL;
  free(file_list->path);
  file_list->path = NULL;

  return;
}

// wrapper function for empty_file_list for use with on_exit
void remove_file_list_on_close(int status, void * file_list_v)
{

  empty_file_list((file_list_t *) file_list_v);
  
  return;
}

#ifdef VID

//checks if extension is that of a video file
int check_extension(char * file_name)
{
  static const char * extensions[] = {
    ".mp4",
    ".webm",
    ".mkv",
    ".avi"
  };
  const char * test_ext = ext_rename(file_name);
  int success = 1, i = 0;
  
  for (i = 0; i < ARRAYSIZE(extensions); ++i)
    {
      if (!strcmp(test_ext, extensions[i]))
	{
	  success = 0;
	  break;
	}
    }
  
  return success;
}

//creates preview file from the file at the path file_name, and places at the path preview_name
int create_preview(const char * file_name, const char * preview_name)
{
  video_thumbnailer * thumbnail = video_thumbnailer_create();
  
  thumbnail->thumbnail_size = 512;
  thumbnail->seek_percentage = 3;
  thumbnail->overlay_film_strip = 1;

  video_thumbnailer_generate_thumbnail_to_file(thumbnail,
					       file_name,
					       preview_name);

  video_thumbnailer_destroy(thumbnail);

  return(0);  
}

//converts a md5 sum to a human readable hex string
char * create_digest_string(const unsigned char * digest)
{
  char * digest_string = malloc(sizeof(char) * 65);
  int i;
  
  for (i = 0; i < MD5_DIGEST_LENGTH; ++i)
    {
      sprintf((digest_string + (2*i)), "%02x", digest[i]);
    }
  *(digest_string + 64) = '\0';
  
  return digest_string;
}

#define create_digest_string_from_string(path) create_digest_string(MD5((const unsigned char *)path, strlen(path), NULL))

//checks for existing preview file for given file, creates it if it doesn't exist, and returns the path to the preview
char * find_preview(const char * file_name)
{
  struct stat vid_stat, preview_stat;
  char * preview_name = create_digest_string_from_string(file_name);
  preview_name = str_append(&preview_name, ".png");
  char * preview_path = path_cat(getenv("HOME"),CACHE_DIR_LOCATION);
  preview_path = str_append(&preview_path, preview_name);
  free(preview_name);
  
  if (access(preview_path, F_OK) == 0)
    {
      int diff_time = 0;
      stat(file_name, &vid_stat);
      stat(preview_path, &preview_stat);

      diff_time = vid_stat.st_mtime - preview_stat.st_mtime;
      
      if (diff_time <= 0)
	return preview_path;
      else
	delete_preview(preview_path);
    }

  create_preview(file_name, preview_path);
  return preview_path;
}

//deletes preview file specified by file_name
int delete_preview(char * file_name)
{
  int success = 0;

  if ((success = unlink(file_name)) != 0)
    printf("unable to delete preview file\n");

  return(success);
}

#endif
