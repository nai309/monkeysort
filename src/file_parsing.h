#ifndef FILE_PARSING_H
#define FILE_PARSING_H
#include "defs.h"
#include <sys/stat.h>

typedef struct {
  int len;
  int pos;
  int rem;
} status_t;

typedef struct {
  time_t mtime;
  char *d_name;
} file_entry;

typedef struct {
  char * path;
  file_entry ** namelist;
  status_t list_stat;
} file_list_t;

#define CURR_FILE(list) (*(list->namelist + list->list_stat.pos))
#define CURR_FILE_NAME(list) (CURR_FILE(list)->d_name)

#define file_list_alloc malloc(1 * sizeof(file_list_t))

void index_dir(file_list_t * file_list, const int direction);
char *check_dir(char **path);
char *check_dir_m(const char *path);
char *path_cat(const char *path, const char *file);
char *ver_name(const char *oldname, const int ver);
char *ext_rename(char *name);
int copy_name(char *path, char *filename, char *dest, char *newname);
int move_name(char *path, char *filename, char *dest, char *newname);
void file_list_conv(file_list_t * file_list, struct dirent **dirlist);
int time_comp(const void *a, const void *b);
int time_comp_up(const void *a, const void *b);
int filter_check(const struct dirent * dir);
int fill_file_list(const char * src_path, file_list_t * file_list, const int start, const int time);
void empty_file_list(file_list_t * file_list);
void remove_file_list_on_close(int status, void * file_list_v);

#ifdef VID
int check_extension(char * file_name);
int create_preview(const char * file_name, const char * preview_name);
char * find_preview(const char * file_name);
int delete_preview(char * file_name);
#endif

#endif //FILE_PARSING_H
