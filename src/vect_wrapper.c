#include "defs.h"
#include "vect_wrapper.h"
#include "file_parsing.h"
#include "dests.h"
#include "file_io.h"
#include "misc.h"
#include "img.h"


extern status_t * curr_list_stat;

#define FILE_LIST (file_list_current(file_vect_list))

int file_list_decon(void * item)
{
  file_list_t * file_list = (file_list_t *) item;

  empty_file_list(file_list);

  free(file_list);

  return 0;
}

int dest_list_decon(void * item)
{
  dest_t * dest_list = (dest_t *) item;

  empty_dest_list(dest_list);

  free(dest_list);

  return 0;
}

dest_t fill_extra_dests(char * dest_file)
{
  int def = 0;
  dest_t dest_list;

  int len = 0, i;
  char **dests = str_list_from_file(dest_file, &len),
    *current_wd = NULL,
    *list_wd = NULL,
    *last_chr = NULL;
  dest_list.count = len;
  
  current_wd = getcwd(NULL, 0);
  list_wd = realpath(dest_file, NULL);
  if (list_wd == NULL)
    {
      printf("could not read destination file: %s, exiting\n", dest_file); //make into a more detailed error statement
      exit(1);
    }
  last_chr = strrchr(list_wd, '/');
  *(last_chr + 1) = '\0';
  chdir(list_wd);
  
  dest_list.dests = dest_dir_fmt(dests, dest_list.count);
  dest_list.destp = destp_fmt(dests, dest_list.count);
  
  chdir(current_wd);
  free(list_wd);
  free(current_wd);
  
  for (i = 0; i < len; ++i)
    free(*(dests + i));
  free(dests);
  
  if (def == 1) free(dest_file);
  
  return dest_list;
}


int dest_vect_list_fill(vect_list_t * dest_vect_list, int argc, char ** argv, svect_t * dvect)
{
  vect_list_append(dest_vect_list, (void *) dest_list_alloc);
  *(dest_list_current(dest_vect_list)) = fill_dests(argc, argv, (dvect->list != NULL ? svect_item(dvect, 0) : NULL));

  for (int i = 1; i < dvect->count; ++i)
    {
      dest_t * dest_list = dest_list_alloc;
      vect_list_append(dest_vect_list, (void *) dest_list);
      *dest_list = fill_extra_dests(svect_item(dvect, i));
    }

  return 0;
}

void index_file_vect_dir(vect_list_t * file_vect_list, const int direction)
{
  FILE_LIST->list_stat.pos += direction;
  
  if (FILE_LIST->list_stat.pos > (FILE_LIST->list_stat.len - 1) ||
      FILE_LIST->list_stat.pos < 0)
    {
      FILE_LIST->list_stat.pos -= direction;
      if (vect_list_index(file_vect_list, direction) == 1)
	{
	  exit(0);
	}
      else
	{
	  curr_list_stat = &((FILE_LIST)->list_stat);
	  return;
	}
    }
  
  while (CURR_FILE(FILE_LIST)->mtime == 1)
    {
      if (direction == 0)
	FILE_LIST->list_stat.pos += 1;
      else
	FILE_LIST->list_stat.pos += direction;
      if (FILE_LIST->list_stat.pos > (FILE_LIST->list_stat.len - 1) ||
        FILE_LIST->list_stat.pos < 0)
	{
	  if (direction == 0)
	    FILE_LIST->list_stat.pos -= 1;
	  else
	    FILE_LIST->list_stat.pos -= direction;
	  if (vect_list_index(file_vect_list, direction) == 1)
	    {
	      exit(0);
	    }
	  else
	    {
	      curr_list_stat = &((FILE_LIST)->list_stat);
	      return;
	    }
	}
    }
  
  return;
}

void next_file_vect_img(vect_list_t * file_vect_list, int direction)
{
  char *filepath = NULL;
  int test;

  #ifdef VID
  if (on_vid == 1)
    {
      //delete_preview(img_window.path);
      on_vid = 0;
    }
  #endif
  
  do {
    index_file_vect_dir(file_vect_list, direction);
    filepath = path_cat(FILE_LIST->path, CURR_FILE_NAME(FILE_LIST));
    if ((test = test_img(filepath)) == 2)
      {
	#ifdef VID
	if ((test = check_extension(filepath)) == 0)
	  on_vid = 1;
	else
	  (CURR_FILE(FILE_LIST))->mtime = 1;
	#else
	(CURR_FILE(FILE_LIST))->mtime = 1;
	#endif
      }
    free(filepath);
  } while (test > 0);

  if (img_window.path) free(img_window.path);
  #ifdef VID
  if (on_vid == 1)
    {
      filepath = path_cat(FILE_LIST->path, CURR_FILE_NAME(FILE_LIST));
      img_window.path = find_preview(filepath);
      free(filepath);
    }
  else
    img_window.path = path_cat(FILE_LIST->path, CURR_FILE_NAME(FILE_LIST));
  #else
  img_window.path = path_cat(FILE_LIST->path, CURR_FILE_NAME(FILE_LIST));
  #endif

  img_set_title(CURR_FILE_NAME(FILE_LIST));

  mouse_delta.x = 0; mouse_delta.y = 0;
  old_delta.x = 0; old_delta.y = 0;
  zoom_factor = 1;
  
  update_render = 1;
  
  return;
}

void print_vfcount(int status, void * arg)
{
  int i;
  vect_list_t * file_vect_list = (void *) arg;

  if (!(flag & QUIET))
    {
      for (i = 0; i < file_vect_list->count; ++i)
	{
	  printf("file list #%d: %s\n", i + 1, ((file_list_t *) vect_list_item(file_vect_list, i))->path);
	  print_count(status, (file_list_t *) vect_list_item(file_vect_list, i));
	}
    }

  return;
}
