#ifndef CONTROLS_H
#define CONTROLS_H
#include "defs.h"
#include <X11/keysym.h>
#include <X11/Xutil.h>
#include "file_parsing.h"
#include "dests.h"
#include "vect_wrapper.h"

typedef enum control_tag {
  CYCLE_UP_K,
  CYCLE_DOWN_K,
  DEST_KEYS_K,
  LIST_K,
  LINE_K,
  CHANGE_K,
  ADD_K,
  REMOVE_K,
  SEEK_K,
  EXIT_K,
  NEW_K,
  SCROLL_K,
  PRINT_K,
  SINGLE_K,
  MULTI_K,
  MOD_RENAME_K,
  MOD_COPY_K,
  MOD_COPY_HOLD_K,
  REVMULTI_K,
  RE_NAME_K,
  DELETE_K,
  DF_PRINT_K,
  UNDO_K,
  D_WINDOW_K,
  CHANGE_FLIST_UP_K,
  CHANGE_FLIST_DOWN_K,
  CHANGE_DLIST_UP_K,
  CHANGE_DLIST_DOWN_K,
  ZOOM_IN_K,
  ZOOM_OUT_K,
  OVERWRITE_K,
  VERSION_K,
  EXTEN_K,
  #ifdef VID
  PLAY_K,
  #endif
  NULL_K
} control_tag_t;


typedef enum exts {
  UP = -1,
  EXT_NONE = 0,
  DOWN = 1,
  LIST,
  LINE,
  CHANGE,
  ADD,
  REMOVE,
  PRINT,
  SINGLE,
  MULTI,
  MOD_RENAME,
  MOD_COPY,
  MOD_COPY_HOLD,
  REVMULTI,
  RE_NAME,
  DELETE,
  DF_PRINT,
  MOD_OVERWRITE,
  MOD_VERSION,
  MOD_EXTEN,
  #ifdef VID
  PLAY,
  #endif
  LAST_CONTROL
} exts_t;

typedef enum modifiers {
  MOD_NONE = 0,
  MOD_SHIFT = 1,
  MOD_CTRL = 1 << 1,
  MOD_ALT = 1 << 2,
  MOD_SUPER = 1 << 3
} mod_t;

typedef struct {
  vect_list_t * file_vect_list;
  vect_list_t * dest_vect_list;
} input_arg_list_t;

struct control { //potentially add a union for extra data
  unsigned int key;
  mod_t mod;
  int (*action)(input_arg_list_t arg_list, exts_t ext);
  exts_t ext;
  control_tag_t tag;
};

extern unsigned int dest_keys[DESTMAX];
extern struct control controls[];

void control_sizes();
mod_t convert_mod(const unsigned int state);

#endif //CONTROLS_H
