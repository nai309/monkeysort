#ifndef IMG_H
#define IMG_H
#include "defs.h"
#include "display.h"
#include "file_parsing.h"

typedef struct {
  window_t window;
  Imlib_Image img_buffer;
  char * path;
} img_window_t;

extern img_window_t img_window;

void img_create_window(char * title);
void img_destroy_window();
char *titling(const char *file);
int img_set_title(const char * file);
int render_background(Imlib_Image * buffer, const int r, const int g, const int b);
rect_t scale_image(const rect_t src_rect, const rect_t dest_rect);
void zoom_image(const int direction);
int loading_img(const char *path);
int test_img(const char * path);
int render_img();

#endif //IMG_H
