#ifndef INPUT_H
#define INPUT_H
#include "defs.h"
#include "input_cont.h"

void get_input(vect_list_t * file_vect_list, vect_list_t *dest_vect_list);

#endif //INPUT_H
