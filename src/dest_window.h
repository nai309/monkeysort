#ifndef DEST_WINDOW_H
#define DEST_WINDOW_H
#include "defs.h"
#include "display.h"
#include "dests.h"
#include "img.h"

//#define LIST_COLOR (SDL_Color) {255, 255, 255, 0}

extern int dest_window_state;

int dest_window_redrw(dest_t * dests);
int dest_window_toggle();

#endif //DEST_WINDOW_H
