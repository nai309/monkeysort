#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include "defs.h"
#include <ctype.h>
#include "settings.h"
#include "file_parsing.h"
#include "file_io.h"
#include "input.h"

#define INC 4

char * def_dest_file_name = DEF_DEST_FILE_NAME;
char delim = DELIM;
char * default_font_loc = DEFAULT_FONT_LOC;
char * default_font = DEFAULT_FONT;
//int destmax = DESTMAX;
int histmax = HISTMAX;
int undomax = UNDOMAX;
int imgcache = IMGCACHE;
int fontcache = FONTCACHE;
double zoom_constant = ZOOM_CONSTANT;

static struct tag_conv control_tags[] = {
  { DEST_KEYS_K, "dest_keys" },
  { CYCLE_UP_K, "cycle-up" },
  { CYCLE_DOWN_K, "cycle-down" },
  { LIST_K, "list" },
  { LINE_K, "line" },
  { CHANGE_K, "change" },
  { ADD_K, "add" },
  { REMOVE_K, "remove-dest" },
  { SEEK_K, "seek" },
  { EXIT_K, "exit" },
  { NEW_K, "new" },
  { SCROLL_K, "scroll" },
  { PRINT_K, "print" },
  { MOD_RENAME_K, "mod_rename" },
  { MOD_COPY_K, "mod_copy" },
  { MOD_COPY_HOLD_K, "mod_copy_hold" },
  { SINGLE_K, "single" },
  { MULTI_K, "multi" },
  { REVMULTI_K, "revmulti" },
  { RE_NAME_K, "rename" },
  { DELETE_K, "delete" },
  { DF_PRINT_K, "df_print" },
  { UNDO_K, "undo" },
  { D_WINDOW_K, "dest-window" },
  { CHANGE_FLIST_UP_K, "change-file-list-up" },
  { CHANGE_FLIST_DOWN_K, "change-file-list-down" },
  { CHANGE_DLIST_UP_K, "change-dest-list-up" },
  { CHANGE_DLIST_DOWN_K, "change-dest-list-down" },
  { ZOOM_IN_K, "zoom-in" },
  { ZOOM_OUT_K, "zoom-out" },
  { OVERWRITE_K, "overwrite" },
  { VERSION_K, "version" },
  #ifdef VID
  { PLAY_K, "play" },
  #endif
  { EXTEN_K, "extension" }
};

static struct tag_conv settings_tags[] = {
    { DEF_DEST_FILE_NAME_O, "default_destination_file_name" },
    { DELIM_O, "delimiter" },
    { DEFAULT_FONT_LOC_O, "font_location" },
    { DEFAULT_FONT_O, "font" },
    { HISTMAX_O, "maximum_history_length" },
    { UNDOMAX_O, "maximum_undo_stack_legnth" },
    { IMGCACHE_O, "imlib2_imgcache_size" },
    { FONTCACHE_O, "imlib2_fontcache_size" },
    #ifdef VID
    { VIDEO_PLAYER_O, "video_player" },
    #endif
    { ZOOM_CONSTANT_O, "zoom_constant" }
};

static struct tag_conv options_tags[] = {
  { SORT_MODE_FL, "sorting_mode" },
  { MOVE_BEHAVIOR_FL, "move_behavior" },
  { QUIET_FL, "quiet_mode" },
  { NONUM_FL, "nonum" },
  { VERBOUS_FL, "verbous" },
  { NO_DEST_FL, "no_default_dest_file" },
  { PULL_UP_TEXT_FL, "pull_up_text_box" }
};

static category_t categories[] = {
  {
    .category_str = "[keys]",
    .handler = set_control,
    .writer = write_controls,
    .tag_len = ARRAYSIZE(control_tags),
    .tags = control_tags
  },
  {
    .category_str = "[settings]",
    .handler = set_setting,
    .writer = write_settings,
    .tag_len = ARRAYSIZE(settings_tags),
    .tags = settings_tags
  },
  {
    .category_str = "[options]",
    .handler = set_option,
    .writer = write_options,
    .tag_len = ARRAYSIZE(options_tags),
    .tags = options_tags
  }
};

FILE *open_conf(char *config_path) //needs to be improved with more error checking
{
  char *real_config_path = NULL;
  FILE *conf_file = NULL;

  if (config_path)
    {
      real_config_path = realpath(config_path, NULL);
      conf_file = fopen(real_config_path, "r");
      free(real_config_path);
    }
  
  if (!conf_file) return(NULL);
  
  return(conf_file);
}

//convert string from config file to conf tag enum value
int str_to_tag(const char *tag_string, category_t * category)
{
  int i;

  for (i = 0; i < category->tag_len; ++i)
    {
      if (!strcmp(tag_string, category->tags[i].tag_str))
	return category->tags[i].tag;
    }
  
  printf("invalid config tag\n");

  return(-1);
}

int line_tok(char *conf_line, category_t * category)
{
  char *token = NULL, *token_list = NULL;
  int token_tag;
  int i;

  if (!conf_line) return(1);
  if (!category) return(1);
  
  token = strtok_r(conf_line, " ", &token_list);

  token_tag = str_to_tag(token, category);

  for (i = 0; i < category->tag_len; i++)
    {
      if (token_tag == category->tags[i].tag)
	{
	  category->handler(&token_list, token_tag);
	  break;
	}
    }

  return(0);
}

category_t * type_string_conv(const char * tag_string) 
{
  uint i;

  for (i = 0; i < ARRAYSIZE(categories); ++i)
    {
      if (!strcmp(tag_string, categories[i].category_str))
	return &(categories[i]);
    }
  
  printf("invalid config category\n");

  return(NULL);
}


int select_conf(char * conf_line)
{
  static category_t * category = NULL;

  if (strlen(conf_line) == 0)
    {
      return(1);
    }

  if (*(conf_line + 0) == '[')
    {
      category = type_string_conv(conf_line);
    }
  else
    {
      line_tok(conf_line, category);
    }
  
  return(0);
}

char * trim_comments(const char * line)
{
  char * line_end = strchrnul(line, COMMENT_CHAR);
  int length = line_end - line, i = length;
  char * trimmed_line = strndup(line, length);

  if (length > 0)
    while (isblank(*(trimmed_line + i - 1)))
      {
	--i;
      }

  *(trimmed_line + i) = '\0';
  
  return trimmed_line;  
}

int conf_set(char *conf_path)
{
  FILE * conf_file = NULL;
  char * conf_buffer = NULL, * trimmed_buffer = NULL;
  
  conf_file = open_conf(conf_path);

  if (conf_file == NULL) { return(0); }

  while (1)
    {
      conf_buffer = read_file_line(conf_file);
      if (conf_buffer == NULL) break;
      trimmed_buffer = trim_comments(conf_buffer);
      P_FREE(conf_buffer);
      select_conf(trimmed_buffer);
      if (trimmed_buffer != NULL) {free(trimmed_buffer); trimmed_buffer = NULL;}
    }
  
  fclose(conf_file);
  
  return(0);
}

void add_mod(const char * mod_str, int * mods)
{
  char mod_char = *(mod_str + 0);

  switch (mod_char)
    {
    case 'C':
      *mods |= MOD_CTRL;
      break;
    case 'S':
      *mods |= MOD_SHIFT;
      break;
    case 'M':
      *mods |= MOD_ALT;
      break;
    case 's':
      *mods |= MOD_SUPER;
      break;
    default:
      printf("invalid modifier: %s\n", mod_str);
      break;
    }

  return;
}

char * set_mods(char * key_str, int * mods)
{
  char * token = NULL, * old_token = NULL, * token_list = NULL;

  *mods = MOD_NONE;
  
  token = strtok_r(key_str, "-", &token_list);
  old_token = token;

  do
    {
      token = strtok_r(NULL, "-", &token_list);
      if (token != NULL)
	{
	  add_mod(old_token, mods);
	  old_token = token;
	}
          }
  while (token != NULL);
  
  return(old_token);
}

//not reentrant safe
char * get_mods(const int mods)
{
  static char mod_str[10] = "";
  strcpy(mod_str, "");

  if (mods & MOD_CTRL)
    strcat(mod_str, "C-");
  if (mods & MOD_SUPER)
    strcat(mod_str, "s-");
  if (mods & MOD_ALT)
    strcat(mod_str, "M-");
  if (mods & MOD_SHIFT)
    strcat(mod_str, "S-");
  
  return(mod_str);
}

int set_control(char **token_list, int tag)
{
  int i;
  unsigned int hold_key;
  int hold_mod = 0;
  char * token = NULL;

  if (tag == DEST_KEYS_K)
    {
      for (i = 0; i < DESTMAX; i++)
	{
	  token = strtok_r(NULL, ",", token_list);
	  if (!token) break;
	  hold_key = XStringToKeysym(token);
	  if (hold_key == NoSymbol)
	    {
	      printf("invalid keyname in config\n");
	      break;
	    }
	  dest_keys[i] = hold_key;
	}
      return(0);
    }
  
  token = strtok_r(NULL, ",", token_list);
  token = set_mods(token, &hold_mod);
  if (!token) return(0);
  hold_key = XStringToKeysym(token);
  if (hold_key == NoSymbol)
    {
      printf("invalid keyname in config\n");
      return(1);
    }
  for (i = 0; i < control_size; i++)
    {
      if (tag == controls[i].tag)
	{
	  controls[i].mod = hold_mod;
	  controls[i].key = hold_key;
	  break;
	}
    }
  
  return(0);
}

int write_controls(FILE * stream, struct category * cat)
{
  int i, j;
  
  fprintf(stream, "%s\n", cat->category_str);
  fprintf(stream, "%s ", cat->tags[0].tag_str);
  
  for (i = 0; i < DESTMAX - 1; i++)
    {
      fprintf(stream, "%s,", XKeysymToString(dest_keys[i]));
    }
  fprintf(stream, "%s\n", XKeysymToString(dest_keys[i]));
  
  for (i = 1; i < cat->tag_len; ++i)
    {
      fprintf(stream, "%s ", cat->tags[i].tag_str);
      
      for (j = 0; j < control_size; ++j) 
	if (cat->tags[i].tag == controls[j].tag)
	  {
	    fprintf(stream, "%s%s\n",
		    get_mods(controls[j].mod),
		    XKeysymToString(controls[j].key));
	  }
    }
  
  return(0);
}

int set_setting(char **token_list, int tag)
{
  char * token = strtok_r(NULL, ",", token_list);

  if (!token) return(1);
  
  switch (tag)
    {
    case DEF_DEST_FILE_NAME_O:
      def_dest_file_name = strdup(token);
      break;
    case DELIM_O:
      delim = *(token);
      break;
    case DEFAULT_FONT_LOC_O:
      default_font_loc = strdup(token);
      break;
    case DEFAULT_FONT_O:
      default_font = strdup(token);
      break;
    case HISTMAX_O:
      histmax = atoi(token);
      break;
    case UNDOMAX_O:
      undomax = atoi(token);
      break;
    case IMGCACHE_O:
      imgcache = atoi(token);
      break;
    case FONTCACHE_O:
      fontcache = atoi(token);
      break;
#ifdef VID
    case VIDEO_PLAYER_O:
      video_player = strdup(token);
      break;
#endif
    case ZOOM_CONSTANT_O:
      zoom_constant = atof(token);
    default:
      break;
    }

  
  return(0);
}

int write_settings(FILE * stream, struct category * cat)
{
  int i;
  
  fprintf(stream, "%s\n", cat->category_str);
  
  for (i = 0; i < cat->tag_len; ++i)
    {
      fprintf(stream, "%s ", cat->tags[i].tag_str);
      
      switch (cat->tags[i].tag)
	{
	case DEF_DEST_FILE_NAME_O:
	  fprintf(stream, "%s\n", def_dest_file_name);
	  break;
	case DELIM_O:
	  fprintf(stream, "%c\n", delim);
	  break;
	case DEFAULT_FONT_LOC_O:
	  fprintf(stream, "%s\n", default_font_loc);
	  break;
	case DEFAULT_FONT_O:
	  fprintf(stream, "%s\n", default_font);
	  break;
	case HISTMAX_O:
	  fprintf(stream, "%i\n", histmax);
	  break;
	case UNDOMAX_O:
	  fprintf(stream, "%i\n", undomax);
	  break;
	case IMGCACHE_O:
	  fprintf(stream, "%i\n", imgcache);
	  break;
	case FONTCACHE_O:
	  fprintf(stream, "%i\n", fontcache);
	  break;
#ifdef VID
	case VIDEO_PLAYER_O:
	  fprintf(stream, "%s\n", video_player);
	  break;
#endif	  
	case ZOOM_CONSTANT_O:
	  fprintf(stream, "%lf\n", zoom_constant);
	default:
	  break;
	}
      
    }

  return(0);
}

int set_option(char **token_list, int tag)
{
  char * token = strtok_r(NULL, ",", token_list);
  int hold = -1;
  
  if (!token) return(1);

  if (!strcmp("on", token))
    hold = 1;
  else if (!strcmp("off", token))
    hold = 0;
	
  
  switch (tag)
    {
    case SORT_MODE_FL:
      if (!strcmp("modified", token))
	time = 1;
      else if (!strcmp("recent", token))
	time = 2;
      else if (!strcmp("alphabetic", token))
	time = 0;
      break;
    case MOVE_BEHAVIOR_FL:
      if (!strcmp("overwrite", token))
	flag = (flag | OVER) & ~RENAME;
      else if (!strcmp("rename", token))
	flag = (flag | RENAME) & ~OVER;
      else if (!strcmp("no-clobber", token))
	flag &= ~RENAME & ~OVER;
      break;
    case QUIET_FL:
      if (hold == 1)
	flag |= QUIET;
      else if (hold == 0)
	flag &= ~QUIET;
      break;
    case NONUM_FL:
      if (hold == 1)
	flag |= NONUM;
      else if (hold == 0)
	flag &= ~NONUM;
      break;
    case VERBOUS_FL:
      if (hold == 1)
	flag |= VERBOUS;
      else if (hold == 0)
	flag &= ~VERBOUS;
      break;
    case NO_DEST_FL:
      if (hold == 1)
	flag |= USE_DEST;
      else if (hold == 0)
	flag &= ~USE_DEST;
      break;
    case PULL_UP_TEXT_FL:
      if (hold == 1)
	flag |= Q_TEXT;
      else if (hold == 0)
	flag &= ~Q_TEXT;
      break;
    default:
      break;
    }

  return(0);
}

static void print_on_off(FILE * stream, const uint8_t onoff)
{
  if (onoff)
    fprintf(stream, "on\n");
  else
    fprintf(stream, "off\n");
  
  return;
}

int write_options(FILE * stream, struct category * cat)
{
  int i;
  
  fprintf(stream, "%s\n", cat->category_str);
  
  for (i = 0; i < cat->tag_len; ++i)
    {
      fprintf(stream, "%s ", cat->tags[i].tag_str);
      
      switch (cat->tags[i].tag)
	{
	case SORT_MODE_FL:
	  if (time == 1)
	    fprintf(stream, "modified\n");
	  else if (time == 2)
	    fprintf(stream, "recent\n");
	  else if (time == 0)
	    fprintf(stream, "alphabetic\n");
	  break;
	case MOVE_BEHAVIOR_FL:
	  if (flag & OVER)
	    fprintf(stream, "overwrite\n");
	  else if (flag & RENAME)
	    fprintf(stream, "rename\n");
	  else 
	    fprintf(stream, "no-clobber\n");
	  break;
	case QUIET_FL:
	  print_on_off(stream, flag & QUIET);
	  break;
	case NONUM_FL:
	  print_on_off(stream, flag & NONUM);
	  break;
	case VERBOUS_FL:
	  print_on_off(stream, flag & VERBOUS);
	  break;
	case NO_DEST_FL:
	 print_on_off(stream, flag & USE_DEST);
	  break;
	case PULL_UP_TEXT_FL:
	  print_on_off(stream, flag & Q_TEXT);
	  break;
 	default:
	  break;
	}
    }

  return(0);
}
  
int generate_default_conf(char *conf_path)
{
  uint i;
  char *real_config_path = NULL;
  FILE *conf_file = NULL;

  if (conf_path)
    //real_config_path = realpath(conf_path, NULL); //fix
    real_config_path = strdup(conf_path);
  else 
    real_config_path = path_cat(getenv("HOME"), USER_CONF_LOCATION);

  if (access(real_config_path, F_OK) == -1)
    {
      conf_file = fopen(real_config_path, "w");

      if (!conf_file)
	{
	  printf("unable to create default conf file,failed to open file at: %s\n", real_config_path);
	  free(real_config_path);
	  return(1);
	}

      for (i = 0; i < ARRAYSIZE(categories); ++i)
	{
	  categories[i].writer(conf_file, &(categories[i]));
	  fprintf(conf_file, "\n");
	}
      
     fclose(conf_file);
    }
  free(real_config_path);
  
  return(0);
}

//lists contents of current configuration to stdout
int list_conf()
{
  uint i;
  
  for (i = 0; i < ARRAYSIZE(categories); ++i)
    {
      categories[i].writer(stdout, &(categories[i]));
    }
  
  return(0);
}
