#ifndef TEXT_H
#define TEXT_H
#include "defs.h"
#include "display.h"
#include "directory_completion.h"
#include "str_utils.h"


#define TEXT_CANCEL_KEY XK_c
#define TEXT_COPY_KEY XK_w
#define TEXT_PASTE_KEY XK_y

#define TEXT_WIN_DEF_WIDTH(disp_w) ((disp_w)/4 + (disp_w)/8)
#define TEXT_WIN_DEF_HEIGHT(disp_h) ((disp_h)/16)
//#define TEXT_MIN_WIDTH(rect_w, disp_w) (rect_w > TEXT_WIN_DEF_WIDTH(disp_w) ? rect_w : TEXT_WIN_DEF_WIDTH(disp_w))
//#define TEXT_MIN_HEIGHT(rect_h, disp_h) (rect_h > TEXT_WIN_DEF_HEIGHT(disp_h) ? rect_h : TEXT_WIN_DEF_HEIGHT(disp_h))
//#define TEXT_LINE_MAX(disp_w) (TEXT_WIN_DEF_WIDTH(disp_w) - (TEXT_WIN_DEF_WIDTH(disp_w))/10)

#define TEXT_MIN_WIDTH(rect_w, disp_w) (rect_w > (disp_w) ? rect_w : (disp_w))
#define TEXT_MIN_HEIGHT(rect_h, disp_h) (rect_h > (disp_h) ? rect_h : (disp_h))
#define TEXT_LINE_MAX(disp_w) ((disp_w) - ((disp_w)/10))

#define ISLATINKEY(keysym) \
  (((KeySym)(keysym) >= XK_space) && ((KeySym)(keysym) <= XK_asciitilde))

void text_create_window(const char *title);
int render_text();
svect_t * gen_message_str(const char * text);
svect_t * gen_tab_str(svect_t * dir_list, const char * text);
rect_t get_message_rect(const Imlib_Font font, const svect_t * message);
Imlib_Image redrw(svect_t * message);
int completion(char ** text, svect_t * dir_list, char ** prefix);
char *text_in (char *title, int hist_n);

#endif //TEXT_H
