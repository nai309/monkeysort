#ifndef DEFS_H
#define DEFS_H
#if HAVE_CONFIG_H
#include "config.h"
#endif
#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
//#include <stdbool.h>
#include <sys/types.h>
#include <linux/limits.h>
#include <dirent.h>
#include <errno.h>
#include <X11/Xlib.h>
#ifdef VID
#include <sys/wait.h>
#include <sys/time.h>
#include <openssl/md5.h>
#include <libffmpegthumbnailer/videothumbnailerc.h>
#endif

#define PRGM_NAME PACKAGE_NAME
#define SYS_DIR_LOCATION  "/usr/share/" PRGM_NAME  "/"
#define DEF_CONF_NAME PRGM_NAME ".conf"
#define DEF_CONF_LOCATION SYSCONFDIR "/" DEF_CONF_NAME
#define USER_DIR_LOCATION ".config/" PRGM_NAME "/"
#define CACHE_DIR_LOCATION ".cache/" PRGM_NAME "/"
#define USER_CONF_LOCATION USER_DIR_LOCATION DEF_CONF_NAME
#define DEF_DEST_FILE_NAME "dest_list.txt"

#define DELIM '|'

#define DEFAULT_FONT_LOC "/usr/share/feh/fonts"
#define DEFAULT_FONT "yudit/12"

#define HISTMAX 50
#define UNDOMAX 200

#define IMGCACHE 50
#define FONTCACHE 4

#ifdef VID
#define VIDEO_PLAYER_DEF "mpv -fs"
#endif

#define ZOOM_CONSTANT 1.21 //constant used for rate of zoom

#ifdef VID


#endif

/* don't change values below this line */

#define DESTMAX 30 //don't change for now

#define COMMENT_CHAR '#'

#define NUM_CHECK ((flag & NONUM) ? 10 : 0)

#define ARRAYSIZE(a) (sizeof(a) / sizeof((a)[0]))

#define P_FREE(pointer) free(pointer); pointer = NULL

#define P_LIST_FREE(list, length) for (int index = 0; index < length; ++index) { free(*(list + index)); } P_FREE(list)

#define XY_IN_RECT(x, y, rx, ry, rw, rh) (((x) >= (rx)) && ((y) >= (ry)) && ((x) < ((rx) + (rw))) && ((y) < ((ry) + (rh))))

#define POINT_IN_RECT(point, rect) (((point.x) >= (rect.x)) && ((point.y) >= (rect.y)) && ((point.x) < ((rect.x) + (rect.w))) && ((point.y) < ((rect.y) + (rect.h))))

typedef struct {
  int on;
  int num;
  int pos;
} scroll_t;

typedef struct ver_check {
  int used;
  char * name;
} ver_check_t;

typedef struct {
  int x,
    y;
} point_t;

typedef struct {
  int x,
    y,
    w,
    h;
} rect_t;

enum flags {
  OVER = 1,
  RENAME = 1 << 1,
  EXTEN = 1 << 2,
  QUIET = 1 << 3,
  NONUM = 1 << 4,
  VERBOUS = 1 << 5,
  USE_DEST = 1 << 6,
  Q_TEXT = 1 << 7
};

#define HIST_NUM 4
enum hist_types {
  NULL_HIST,
  DEST_HIST,
  POS_HIST,
  NAME_HIST
};

extern scroll_t scroll;

extern uint8_t flag; //variable to hold the ANDed commandline flags

extern char mod_mode;

extern uint control_size;

extern int dest_window_state;

extern int update_render;

extern point_t mouse_delta, old_delta;

extern double zoom_factor;

extern int time;

//extern declarations for option values
extern char * def_dest_file_name;
extern char delim;
extern char * default_font_loc;
extern char * default_font;
//extern int destmax;
extern int histmax;
extern int undomax;
extern int imgcache;
extern int fontcache;
#ifdef VID
extern char * video_player;
#endif
extern double zoom_constant;

extern char * filter;

#ifdef VID
extern int on_vid;
#endif

#endif //DEFS_H
