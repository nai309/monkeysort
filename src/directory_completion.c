#include "defs.h"
#include "directory_completion.h"


int dir_compare_r(svect_t * src_dir_list, svect_t * dir_list, const char * s_pre)
{
  int len = strlen(s_pre), i;
  (dir_list)->count = 0;
  (dir_list)->list = NULL;

  for (i = 0; i < src_dir_list->count; ++i)
    {
      if (str_prefix_cmp(s_pre, len, *(src_dir_list->list + i)) == 0)
	{
	  svect_append(dir_list, *(src_dir_list->list + i));
	}
    }
  
  return(0);
}

svect_t * dir_compare_m(svect_t * src_dir_list, const char * s_pre)
{
  int len = strlen(s_pre), i;
  svect_t * dir_list = svect_create();

  for (i = 0; i < src_dir_list->count; ++i)
    {
      if (str_prefix_cmp(s_pre, len, *(src_dir_list->list + i)) == 0)
	{
	  svect_append(dir_list, *(src_dir_list->list + i));
	}
    }
  
  return(0);
}

//relies on system having d_type support
int dir_filter(const struct dirent * dir)
{
  if (dir->d_type == DT_DIR)
    return(1);
  else
    return(0);
}

int fill_dir_list_r(svect_t * dir_list, const char * dir_path)
{
  char * real_dir_path = realpath(dir_path, NULL);
  int len, i;
  struct dirent ** dirent_list;

  if (real_dir_path == NULL) return(1);

  len = scandir(real_dir_path, &dirent_list, dir_filter, versionsort);
  free(real_dir_path);
  
  for (i = 0; i < len; ++i)
    {
      if (*((*(dirent_list + i))->d_name) == '.' &&
	  (*((*(dirent_list + i))->d_name + 1) == '\0' ||
	   *((*(dirent_list + i))->d_name + 1) == '.'))
      {
	continue;
      }
      svect_append(dir_list, (*(dirent_list + i))->d_name);
    }

  for (i = 0; i < len; ++i)
    {
      free(*(dirent_list + i));
    }
  free(dirent_list);
  
  return(0);  
}
