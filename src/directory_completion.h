#ifndef DIRECTORY_COMPLETION_H
#define DIRECTORY_COMPLETION_H
#include "defs.h"
#include "str_utils.h"

int dir_compare_r(svect_t * src_dir_list, svect_t * dir_list, const char * s_pre);
svect_t * dir_compare_m(svect_t * src_dir_list, const char * s_pre);
int dir_filter(const struct dirent * dir);
int fill_dir_list_r(svect_t * dir_list, const char * dir_path);

#endif //DIRECTORY_COMPLETION_H
