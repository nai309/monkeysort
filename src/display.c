#include "defs.h"
#include "display.h"

x_disp_t display;

int update_render;

rect_t get_window_size(window_t * window)
{
  rect_t rect;
  XWindowAttributes atts;
  XGetWindowAttributes(display.disp, window->screen, &atts);
  rect.x = atts.x;
  rect.y = atts.y;
  rect.w = atts.width;
  rect.h = atts.height;
  
  return(rect);
}

void close_window(window_t * win)
{
  XDestroyWindow(display.disp, win->screen);

  return;
}
