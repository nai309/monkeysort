#include "defs.h"
#include "misc.h"
#include "input_cont.h"
#include "file_parsing.h"

extern unsigned int dest_keys[DESTMAX];

void list_dests(dest_t *list) //list destinations formatted
{
  int i, nonum = NUM_CHECK;

  if (list->count > 0)
    {
      for (i = nonum; i < (list->count + nonum); ++i)
	printf("%i - %s - %s\n",
	       ((i + 1) - nonum),
	       XKeysymToString(dest_keys[i]),
	       *(list->destp + i));
    }
  else
    printf("no destinations specified\n");
  return;
}

void dest_line(dest_t *list) //list destinations copyable
{
  int i;
  char **hold = NULL;

  hold = list->destp;
  for (i=0; i < list->count; i++)
    {
      printf("\"%s\" ", *(hold + i + NUM_CHECK));
    }
  printf("\n");
  return;
  }

void print_count(int status, void * arg)
{
  file_list_t * file_list = (file_list_t *) arg;

  if (!(flag & QUIET))
    {
      printf("%i - %i\n",
	     file_list->list_stat.pos+1,
	     file_list->list_stat.len
	     );
      printf("%i - %i\n",
	     (file_list->list_stat.pos+1 - file_list->list_stat.rem),
	     (file_list->list_stat.len - file_list->list_stat.rem)
	     );
    }
      
  return;
}

/*
void help_msg() //needs massive updating
{
  printf("usage: img_sorter [OPTIONS] SRC_DIR [DEST_DIR(1-%i)]\n", DESTMAX);
  printf("\noption summary:\n"
	 "h - print this help message\n\n"
	 "o - enable overwriting files when moving to destination\n\n"
	 "a - sort files alphabetically\n\n"
	 "u - sort by mod time from newest to oldest\n\n"
	 "i [SRC_DIR] - uses option argument for source directory\n\n"
	 "s [ARG] - start program at ARG position in the file list\n\n"
	 "r - move and rename mode, when filename already exists, appends (1) to filename and moves to destination\n\n"
	 "e - makes rename operation also allow editing of file extension\n\n"
	 "q - quiet mode, suppresses printing the file list position and size when the program exits\n\n"
	 "n - disables use of the numpad for destination selection, limits primary destinations to the top row number keys and the function keys, and the destination limit to 20\n\n"
	 "f - specify a destination list file to load destinations from\n\n"
	 "l - starts without the destination list window visible\n\n"
	 "w - specifies the directory to use as the working directory for relative file movement and for directory tab completions\n\n"
	 
	 "\ncontrol summary:\n"
	 "UP, DOWN - cycles forward and backward in the file list, also works with MS_WHEEL up and down\n\n"
"LEFT_MOUSE, KP_ENTER - cycles one file forward in the file list\n\n"
	 "KP_[1-0] - moves current file to key's matching destination\n\n");
  printf("[1-0] - same as KP_[1-0], but for destinations 11-%i\n\n", DESTMAX - 10);
  printf("F[1-10] - same as KP_[1-0] and [1-0], but for destinations 21-%i\n\n", DESTMAX);
  printf("ENTER - allows for entering a custom destination for the current file\n"
	 "     use: type directory into terminal, followed by return\n\n"
	 "M - multi ENTER, moves the current file and the (n) files following it to a custom destination\n"
	 "     use: enter number of files, press return, enter destination, press return\n\n"
	 ", - multi ENTER, but for the current file and the (n) files preceding it\n\n");
  printf("A - adds new destination to end of destination list if not already at %i destinations\n", DESTMAX);
  printf("     use: type directory into terminal, followed by return\n\n"
	 "C - changes the directory pointed to by destination (n)\n"
	 "     use: enter number of destination, press return, enter new destination, press return\n\n"
	 "L - lists key and destination matchups\n\n"
	 "K - lists destinations in a copyable format\n\n"
	 "P - prints the filename of the current file to the terminal\n\n"
	 "S - seek to position in file list\n"
	 "     use: enter file list location in terminal, followed by return\n\n"
	 "R - rename file in file list\n"
	 "     use: enter filename in terminal, followed by return\n"
	 "     note: filename should not have a leading slash or path, and it must include the file extension\n\n"
	 "N - creates a new directory with a given name, can be relative, relative directories will be relative to the path the program was run in\n"
	 "     use: enter directory name in terminal, followed by return\n\n"
	 "Q, ESCAPE - exits the program\n\n"
	 );
}  
*/
