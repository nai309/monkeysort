#include "defs.h"
#include "history.h"

hist_list hists[HIST_NUM]; //array of history lists
hist_list short_list[1]; //list of easily accessable destinations

//initializes history lists
int hist_init()
{
  int i;

  short_list->head = malloc(sizeof(hist_node));
  *(short_list->head) = (hist_node) { NULL, NULL, NULL };
  short_list->current = short_list->head;

  (hists + 0)->head = NULL;

  for (i = 1; i < HIST_NUM; ++i)
    {
      (hists + i)->head = malloc(sizeof(hist_node));
      *((hists + i)->head) = (hist_node) { NULL, NULL, NULL };
      (hists + i)->current = (hists + i)->head;
    }

  return 0;
}

//create new history node at head of the history list
int hist_append(hist_list *history)
{
  if (history->total >= histmax)
    {
      hist_node *new_start = history->start->next;
      new_start->prev = NULL;
      free(history->start->text);
      free(history->start);
      history->start = new_start;
    }
  else
    (history->total)++;
  hist_node *old = history->recent;
  history->recent = malloc(sizeof(hist_node));
  *(history->recent) = (hist_node) { NULL, NULL, NULL };
  history->recent->prev = old;
  history->head->prev = history->recent;
  if (old != NULL)
    old->next = history->recent;
  else
    history->start = history->recent;

  return(0);
}

//indexs one position in a history list
hist_node *index_node(hist_node *node, int direction) //not currently used
{
  hist_node *new_pos = NULL;

  if (direction < 0)
    new_pos = node->prev;
  else if (direction > 0)
    new_pos = node->next;

  return new_pos;
}

//travels one position in the history list and sets the current buffer text to the value of the next history node
void hist_trav(int direction, char **text, hist_list *history)
{
  if (history->current == NULL || history->head == NULL)
    return;

  if (direction == 0) //resets current to head
    {
      history->current = history->head;

      return;
    }
  
  if (direction < 0)
    {
      if (history->current->prev != NULL)
	{
	  free(*text);
	  history->current = history->current->prev;
	  *text = strdup(history->current->text);
	}
    }
  else if (direction > 0)
    {
      if (history->current->next != NULL)
	{
	  free(*text);
	  history->current = history->current->next;
	  *text = strdup(history->current->text);
	}
    }

  return;
}

//creates a new history node and stores the passed text into it
void hist_set(char *str, hist_list *history)
{
  if (history->head == NULL)
    return;

  if (history->recent != NULL && history->recent->text != NULL)
    if (!(strcmp(history->recent->text, str)))
      {
	history->current = history->head;
	return;
      }
  
  hist_append(history);
  history->recent->text = strdup(str);
  history->head->prev = history->recent;
  history->current = history->head;
  
  return;
}

void hist_empty_list(hist_list * history)
{
  hist_node * hold = history->head, * rem = NULL;
  
  while (hold != NULL)
    {
      rem = hold;
      hold = hold->prev;
      free(rem->text);
      free(rem);
    }
  
  return;
}

void hist_empty_on_close()
{
  int i;

  for (i = 0; i < HIST_NUM; ++i)
    {
      hist_empty_list((hists + i));
    }
  hist_empty_list(short_list);
  
  return;
}
