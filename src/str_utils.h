#ifndef STR_UTILS_H
#define STR_UTILS_H
#include "defs.h"

#define SVECT_BUFFER 4

typedef struct {
  int count;
  int buffer;
  char ** list;
} svect_t;

#define svect_alloc() malloc(1 * sizeof(svect_t))
#define svect_item(svect, num) *(((svect)->list) + num)
#define svect_truncate(svect, num) svect_remove(svect, ((svect)->count - num), num)
#define svect_empty(svect) svect_truncate(svect, (svect)->count)


//must be assigned to dest when called
#define STR_APPEND_R(dest, src) strcat(realloc(dest, sizeof(char) * ((strlen(dest) + strlen(src) + 1))), src)
#define STR_APPEND(dest, src) dest = STR_APPEND_R(dest, src)
char * str_cat_dup(const char * str1, const char * str2);
int str_prefix_cmp(const char * s_pre, int s_len, const char * str);
char * str_append(char ** dest, const char * src);
svect_t * svect_create();
int svect_append(svect_t * svect, const char * str);
int svect_nappend(svect_t * svect, const char * str, const size_t n);
int svect_remove(svect_t * svect, int num, int dist);


#endif //STR_UTILS_H
