#ifndef VECT_H
#define VECT_H
#include "defs.h"

//consider adding a union for extra data related to the list
typedef struct {
  int count;
  int current;
  int buffer;
  int (*decon)(void * item);
  void ** list;
} vect_list_t;

typedef struct {
  int (*decon)(void * item);
  void * item;
} anon_container;

#define vect_list_item(vect_list, num) *(((vect_list)->list) + num)
#define vect_list_current(vect_list) vect_list_item(vect_list, (vect_list)->current)
#define vect_list_truncate(vect_list, num) vect_list_remove(vect_list, ((vect_list)->count - num), num)
#define vect_list_empty(vect_list) vect_list_truncate(vect_list, (vect_list)->count)

vect_list_t vect_list_init(int (*decon)(void * item));
vect_list_t * vect_list_init_m(int (*decon)(void * item));
int vect_list_append(vect_list_t * vect_list, void * item);
int vect_list_remove(vect_list_t * vect_list, int num, int dist);
int vect_list_index(vect_list_t * vect_list, int direction);
int vect_list_decon(void * item);
int anon_list_decon(void * item);
void remove_vect_list_on_close(int status, void * vect_list_v);

#endif //VECT_H
