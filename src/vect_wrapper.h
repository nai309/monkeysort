#ifndef VECT_WRAPPER_H
#define VECT_WRAPPER_H
#include "defs.h"
#include "vect.h"
#include "str_utils.h"

#define file_list_current(vect_list) ((file_list_t *) vect_list_current(vect_list))
#define dest_list_current(vect_list) ((dest_t *) vect_list_current(vect_list))

extern vect_list_t * fv_list;
extern vect_list_t * dv_list;

int file_list_decon(void * item);
int dest_list_decon(void * item);
int dest_vect_list_fill(vect_list_t * dest_vect_list, int argc, char ** argv, svect_t * dvect);
void index_file_vect_dir(vect_list_t * file_vect_list, const int direction);
void next_file_vect_img(vect_list_t * file_vect_list, int direction);
void print_vfcount(int status, void * arg);

#endif //VECT_WRAPPER_H
