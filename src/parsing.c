#include "defs.h"
#include "parsing.h"
#include "file_parsing.h"
#include "file_io.h"
#include "text.h"
#include "history.h"
#include "undo.h"
#include "controls.h"

extern ver_check_t ver_fix;

int get_num() //gets a number from the user
{
  char *line = NULL;
  int num;

  line = text_in("enter number", POS_HIST);
  if (line == NULL || *(line) == '\n')
    {
      free(line);
      return(0);
    }
  
  num = strtol(line, NULL, 0);
  free(line);

  return(num);
}

int get_dest(char **dest_path)
{
  char * line = NULL;
  
  line = text_in("enter destination", DEST_HIST);
  if (line == NULL || *(line) == '\n')
    {
      free(line);
      return(1);
    }
  
  *dest_path = realpath(line, NULL);
  if (*dest_path == 0)
    {
      free(line);
      printf("invalid input\n");
      printf("%s\n",strerror(errno));
      return(1);
    }
  
  free(line);
  *dest_path = check_dir(dest_path);
  
  return(0);
}

int new_dir()
{
  char *line = NULL;
  int dir_suc;
  
  line = text_in("enter new directory", DEST_HIST);
  if (line == NULL || *(line) == '\n')
    {
      free(line);
      return(1);
    }

  dir_suc = mkdir(line, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

  if (dir_suc == -1)
    {
      printf("%s\n",strerror(errno));
      free(line);
      return(1);
    }

  free(line);

  return(dir_suc);
}

int move(file_list_t * file_list, const int pos, char *dest)
{
  int success = 1;
  file_entry * file = *(file_list->namelist + pos);

  if (mod_mode == 'c' || mod_mode == 'C')
    {
      copy_name(file_list->path, file->d_name, dest, file->d_name);
      if (ver_fix.used == 1)
	{
	  undo_push(COPY, file_list->path, file, dest, ver_fix.name);
	  free(ver_fix.name);
	  ver_fix.used = 0;
	}
      else
	{
	  undo_push(COPY, file_list->path, file, dest, file->d_name);
	}
      if (mod_mode == 'c')
	mod_mode = '0';
    }    
  else if (mod_mode == 'r')
    {
      success = re_name(file_list->path, file, dest);
      mod_mode = '0';
    }
  else
    {
      success = move_name(file_list->path, file->d_name, dest, file->d_name);
      if (success == 0)
	{
	  if (ver_fix.used == 1)
	    {
	      undo_push(MOVE, file_list->path, file, dest, ver_fix.name);
	      free(ver_fix.name);
	      ver_fix.used = 0;
	    }
	  else
	    {
	      undo_push(MOVE, file_list->path, file, dest, file->d_name);
	    }
	}
    }
    
  if (success == 0)
    {
      file->mtime = 1;
      file_list->list_stat.rem++;
    }
  
  return(success);
}

int re_name(char *path, file_entry *file, char *dest)
{
  char *newname = NULL, *ext_name = NULL;
  int success = 1;

  newname = text_in("enter new filename", NAME_HIST);
  if (newname == NULL || *(newname) == '\n')
    {
      free(newname);
      return(1);
    }

  if (!(flag & EXTEN)) {
    ext_name = ext_rename(file->d_name);
    newname = realloc(newname, (strlen(newname) + strlen(ext_name) + 1));
    strcat(newname, ext_name);
  }
  
  success = move_name(path, file->d_name, dest, newname);
  if (success == 0) {
    file->d_name = newname;
    return(0);}
  else
    return(1);
}

int rm_file(file_list_t * file_list, int pos)
{
  char * filepath = NULL, * hold_str = NULL, answer = '\0';
  file_entry * file = *(file_list->namelist + pos);
  int success = 1;

  hold_str = text_in("confirm deletion [y/N]", NULL_HIST);

  if (hold_str != NULL)
    {
      answer = *(hold_str);
      free(hold_str);
    }
  
  if (answer == 'y') // will work with any string starting with y
    { 
      filepath = path_cat(file_list->path, file->d_name);
      
      if ((success = unlink(filepath)) != 0)
	printf("unable to delete file\n");
      
      free(filepath);
      
      if (success == 0)
	{
	  file->mtime = 1;
	  file_list->list_stat.rem++;
	  return(0);
	}
    }

  return(1);
}

int seek(file_list_t * file_list)
{
  char *line = NULL;
  int direct = 1, pos;

  line = text_in("enter filelist position", POS_HIST);
  if (line == NULL || *(line) == '\n')
    {
      free(line);
      return(0);
    }
  
  pos = strtol(line, NULL, 0);
  free(line);
  if (pos > file_list->list_stat.len || pos < 1) { printf("out of range\n"); return(0); }
  
  if (pos - file_list->list_stat.pos < 0)
    direct = UP;
  else if (pos - file_list->list_stat.pos > 0)
    direct = DOWN;

  file_list->list_stat.pos = pos - direct - 1;

  return (direct);
}

int multi_reading(file_list_t * file_list, char * dest_path, int num, const int direction)
{
  int i, hold = 1;
  
  if (num < 1) { printf("number less than one\n"); return(1); }
  
  if (direction == DOWN)
    {
      if (num > 1) undo_mstop();//
      for (i = 0; i < num; i++)
	{
	  if (file_list->list_stat.pos + i > (file_list->list_stat.len - 1) || file_list->list_stat.pos + i < 0)
	    exit(0);
	  if ((*(file_list->namelist + file_list->list_stat.pos + i))->mtime == 1) {num++; continue;}
	  hold = move(file_list, (file_list->list_stat.pos + i), dest_path);
	}
      if (num > 1) undo_mstart();//
    }
  else if (direction == UP)
    {
      if (num > 1) undo_mstop();//
      for (i = 0; i > -(num); i--)
	{
	  if (file_list->list_stat.pos + i > (file_list->list_stat.len - 1) || file_list->list_stat.pos + i < 0)
	    exit(0);
	  if ((*(file_list->namelist + file_list->list_stat.pos + i))->mtime == 1) {num++; continue;}
	  hold = move(file_list, (file_list->list_stat.pos + i), dest_path);
	}
      if (num > 1) undo_mstart();//
    }

  return(hold);
}

// indexes the scroll selection counter by the given number, and handles changes in scroll direction
void index_scroll(const int num)
{
  if (scroll.num == 1 && num == -1)
    scroll.num = -2;
  else if (scroll.num == -2 && num == 1)
    scroll.num = 1;
  else
    scroll.num += num;
  
  return;
}

//does not reset scroll state by itself
int scroll_reading(file_list_t * file_list, char * dest_path)
{
  int i, hold = 1;
  
  if (scroll.num > 0)
    {
      if (scroll.num > 1) undo_mstop();//
      for (i = 0; i < scroll.num; i++)
	{
	  if (scroll.pos + i > (file_list->list_stat.len - 1) || scroll.pos + i < 0)
	    exit(0);
	  if ((*(file_list->namelist + scroll.pos + i))->mtime == 1) {scroll.num++; continue;}
	  hold = move(file_list, (scroll.pos + i), dest_path);
	}
      if (scroll.num > 1) undo_mstart();//
    }
  else if (scroll.num < 0)
    {
      if (-(scroll.num) > 1) undo_mstop();//
      for (i = 0; i > scroll.num; i--)
	{
	  if (scroll.pos + i > (file_list->list_stat.len - 1) || scroll.pos + i < 0)
	    exit(0);
	  if ((*(file_list->namelist + scroll.pos + i))->mtime == 1) {scroll.num--; continue;}
	  hold = move(file_list, (scroll.pos + i), dest_path);
	}
      if (-(scroll.num) > 1) undo_mstart();//
    }

  return(hold);
}

