#include "defs.h"
#include "controls.h"
#include "input_cont.h"

//assigns default values for destination keys
unsigned int dest_keys[DESTMAX] = {
  XK_KP_1,
  XK_KP_2,
  XK_KP_3,
  XK_KP_4,
  XK_KP_5,
  XK_KP_6,
  XK_KP_7,
  XK_KP_8,
  XK_KP_9,
  XK_KP_0,
  XK_1, 
  XK_2, 
  XK_3, 
  XK_4, 
  XK_5, 
  XK_6, 
  XK_7, 
  XK_8, 
  XK_9, 
  XK_0, 
  XK_F1,
  XK_F2,
  XK_F3,
  XK_F4,
  XK_F5,
  XK_F6,
  XK_F7,
  XK_F8,
  XK_F9,
  XK_F10
};

struct control controls[] = { //potentially replace first value with null terminated array
  { XK_Down, MOD_NONE, cycle_cont, DOWN, CYCLE_DOWN_K },
  { XK_Up, MOD_NONE, cycle_cont, UP, CYCLE_UP_K },
  { XK_c, MOD_NONE, change_dest_cont, CHANGE, CHANGE_K },
  { XK_a, MOD_NONE, change_dest_cont, ADD, ADD_K },
  { XK_l, MOD_CTRL, print_cont, LIST, LIST_K },
  { XK_k, MOD_NONE, print_cont, LINE, LINE_K },
  { XK_s, MOD_NONE, seek_cont, EXT_NONE, SEEK_K },
  { XK_q, MOD_NONE, exit_cont, EXT_NONE, EXIT_K },
  { XK_slash, MOD_NONE, scroll_cont, EXT_NONE, SCROLL_K },
  { XK_p, MOD_NONE, print_cont, PRINT, PRINT_K },
  { XK_Return, MOD_NONE, move_cont, SINGLE, SINGLE_K },
  { XK_m, MOD_NONE, move_cont, MULTI, MULTI_K},
  { XK_n, MOD_NONE, new_dir_cont, EXT_NONE, NEW_K },
  { XK_r, MOD_NONE, move_cont, RE_NAME, RE_NAME_K },
  { XK_c, MOD_CTRL, mod_mode_cont, MOD_COPY, MOD_COPY_K },
  { XK_c, MOD_CTRL | MOD_SHIFT, mod_mode_cont, MOD_COPY_HOLD, MOD_COPY_HOLD_K },
  { XK_r, MOD_CTRL, mod_mode_cont, MOD_RENAME, MOD_RENAME_K },
  { XK_m, MOD_CTRL, move_cont, REVMULTI, REVMULTI_K },
  { XK_d, MOD_CTRL, move_cont, DELETE, DELETE_K },
  { XK_p, MOD_CTRL, print_cont, DF_PRINT, DF_PRINT_K },
  { XK_u, MOD_CTRL, undo_cont, EXT_NONE, UNDO_K },
  { XK_l, MOD_NONE, dest_window_cont, EXT_NONE, D_WINDOW_K },
  { XK_x, MOD_CTRL, change_dest_cont, REMOVE, REMOVE_K },
  { XK_Right, MOD_SHIFT, change_file_vect_list, DOWN, CHANGE_FLIST_DOWN_K },
  { XK_Left, MOD_SHIFT, change_file_vect_list, UP, CHANGE_FLIST_UP_K},
  { XK_Right, MOD_NONE, change_dest_vect_list, DOWN, CHANGE_DLIST_DOWN_K },
  { XK_Left, MOD_NONE, change_dest_vect_list, UP, CHANGE_DLIST_UP_K },
  { XK_Up, MOD_SHIFT, zoom_cont, DOWN, ZOOM_IN_K },
  { XK_Down, MOD_SHIFT, zoom_cont, UP, ZOOM_OUT_K },
  { XK_o, MOD_CTRL, mod_mode_cont, MOD_OVERWRITE, OVERWRITE_K },
  { XK_v, MOD_CTRL, mod_mode_cont, MOD_VERSION, VERSION_K },
  #ifdef VID
  { XK_j, MOD_NONE, play_video, PLAY, PLAY_K },
  #endif
  { XK_e, MOD_CTRL, mod_mode_cont, MOD_EXTEN, EXTEN_K }
};

uint control_size;

void control_sizes()
{
  control_size = ARRAYSIZE(controls);
  
  return;
}

mod_t convert_mod(const unsigned int state)
{
  unsigned int san_modifier = (state);
  mod_t mod = 0;
  
  if (san_modifier & ShiftMask)
    mod |= MOD_SHIFT;
  if (san_modifier & ControlMask)
    mod |= MOD_CTRL;
  if (san_modifier & Mod1Mask)
    mod |= MOD_ALT;
  if (san_modifier & Mod4Mask)
    mod |= MOD_SUPER;
  
  return mod;
}
