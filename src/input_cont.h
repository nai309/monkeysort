#ifndef INPUT_CONT_H
#define INPUT_CONT_H
#include "defs.h"
#include "file_parsing.h"
#include "dests.h"
#include "controls.h"

int cycle_cont(input_arg_list_t arg_list, exts_t ext);
int change_dest_cont(input_arg_list_t arg_list, exts_t ext);
int print_cont(input_arg_list_t arg_list, exts_t ext);
int exit_cont(input_arg_list_t arg_list, exts_t ext);
int seek_cont(input_arg_list_t arg_list, exts_t ext);
int new_dir_cont(input_arg_list_t arg_list, exts_t ext);
int scroll_cont(input_arg_list_t arg_list, exts_t ext);
int mod_mode_cont(input_arg_list_t arg_list, exts_t ext);
int undo_cont(input_arg_list_t arg_list, exts_t ext);
int dest_window_cont(input_arg_list_t arg_list, exts_t ext);
int zoom_cont(input_arg_list_t arg_list, exts_t ext);
int move_cont(input_arg_list_t arg_list, exts_t ext);
int dest_cont(input_arg_list_t arg_list, exts_t ext);
int change_file_vect_list(input_arg_list_t arg_list, exts_t ext);
int change_dest_vect_list(input_arg_list_t arg_list, exts_t ext);
#ifdef VID
int play_video(input_arg_list_t arg_list, exts_t ext);
#endif

#endif //INPUT_CONT_H
