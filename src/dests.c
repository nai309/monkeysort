#include "defs.h"
#include "dests.h"
#include "file_parsing.h"
#include "text.h"
#include "file_io.h"


char **dest_dir_fmt(char **args, const int count) //creates list of absolute paths
{
  char **dirs = NULL;
  int i;

  dirs = calloc(DESTMAX, sizeof(args));
  for (i = 0; i < count; i++)
    {
      *(dirs + i + NUM_CHECK) = realpath(*(args + i), NULL);
      if (*(dirs + i + NUM_CHECK) == NULL) 
	{
	  printf("destination %i does not exist\n", (i + 1));
	  exit(1);
	}
      *(dirs + i + NUM_CHECK) = check_dir((dirs + i + NUM_CHECK));
    }
  return(dirs);
}

char **destp_fmt(char **args, const int count) //creates copy of dests from argv
{
  char **dirs = NULL;
  int i;

  dirs = malloc((sizeof(args) * DESTMAX));
  for (i = 0; i < count; i++)
    {
      *(dirs + i + NUM_CHECK) = check_dir_m(*(args + i));
    }
  return(dirs);
}

int change_dest(dest_t *dest_list, int num)
{
  char *line = NULL, *dest_path = NULL;

  if (num <= 0) return(1);

  if (num > DESTMAX) { printf("number out of range\n"); return(1); }

  //appends to end of list if trying to change unallocated positions
  if (num > dest_list->count + NUM_CHECK + 1)
    num = dest_list->count + NUM_CHECK + 1;
  
  line = text_in("enter new destination", DEST_HIST);
  if (line == NULL || *(line) == '\n')
    {
      free(line);
      return(1);
    }
  
  dest_path = realpath(line, NULL);
  if (dest_path == NULL) {
    printf("invalid input\n");
    printf("%s\n",strerror(errno));
    free(line);
    return(1);
  }

  line = check_dir(&line);
  dest_path = check_dir(&dest_path);

  if (num >= dest_list->count + NUM_CHECK)
    {
      dest_list->destp = realloc(dest_list->destp, (num) * sizeof(char *));
      *(dest_list->destp + num - 1) = NULL;
      dest_list->dests = realloc(dest_list->dests, (num) * sizeof(char *));
      *(dest_list->dests + num - 1) = NULL;
      dest_list->count++;
    }

  if (*(dest_list->destp + num - 1) != NULL)
    free(*(dest_list->destp + num - 1));
  if (*(dest_list->dests + num - 1) != NULL)
    free(*(dest_list->dests + num - 1));
  
  *(dest_list->destp + num - 1) = strdup(line);
  free(line);
  *((dest_list->dests) + num - 1) = dest_path;
  
  return(0);
}

void rm_dest(dest_t * dest_list)
{
  if (dest_list->count > 0)
    {
      dest_list->count--;
      
      free(*(dest_list->dests + dest_list->count));
      free(*(dest_list->destp + dest_list->count));
    }

  return;
}

int cat_str_list(char ***list1, const int list1_len, char **list2, const int list2_len)
{
  int i, j;
  
  *list1 = realloc(*list1, (list1_len + list2_len) * sizeof(char *));

  for (i = list1_len, j = 0; i < (list1_len + list2_len); ++i, ++j)
    {
      *(*list1 + i) = *(list2 + j);
    }

  return 0;
}

char * check_for_dest_file()
{
  char * dest_file = NULL,
    * filepath = NULL,
    * current_wd = NULL;

  current_wd = getcwd(NULL, 0);
  filepath = path_cat(current_wd, def_dest_file_name);
  free(current_wd);
  
  if (access(filepath, F_OK) != -1)
    {
      dest_file = strdup(def_dest_file_name);
    }
  free(filepath);
  
  return(dest_file);
}

int append_dests(dest_t *dest_list, char **new_entry, const int num)
{
  char **new_dests = NULL, **new_destp = NULL;

  new_dests = dest_dir_fmt(new_entry, num);
  new_destp = destp_fmt(new_entry, num);

  cat_str_list(&(dest_list->dests), dest_list->count, new_dests, num);
  cat_str_list(&(dest_list->destp), dest_list->count, new_destp, num);
  
  return 0;
}

dest_t fill_dests(const int args_num, char **args, char *dest_file)
{
  int def = 0;
  dest_t dest_list;

  if (dest_file == NULL && !(flag & USE_DEST))
    {
      dest_file = check_for_dest_file();
      def = 1;
    }

  if (dest_file != NULL)
    {
      int len = 0, i;
      char **dests = str_list_from_file(dest_file, &len),
	*current_wd = NULL,
	*list_wd = NULL,
	*last_chr = NULL;
      dest_list.count = len;

      current_wd = getcwd(NULL, 0);
      list_wd = realpath(dest_file, NULL);
      if (list_wd == NULL)
	{
	  printf("could not read destination file: %s, exiting\n", dest_file); //make into a more detailed error statement
	  exit(1);
	}
      last_chr = strrchr(list_wd, '/');
      *(last_chr + 1) = '\0';
      chdir(list_wd);
            
      dest_list.dests = dest_dir_fmt(dests, dest_list.count);
      dest_list.destp = destp_fmt(dests, dest_list.count);

      chdir(current_wd);
      free(list_wd);
      free(current_wd);
      
      for (i = 0; i < len; ++i)
	free(*(dests + i));
      free(dests);
      
      int arg_count = args_num - (optind + 1);
      append_dests(&dest_list, args + (optind + 1), arg_count);
      dest_list.count += arg_count;
    }
  else
    {
      dest_list.count = args_num - (optind + 1);
      dest_list.dests = dest_dir_fmt(args + (optind + 1), dest_list.count);
      dest_list.destp = destp_fmt(args + (optind + 1), dest_list.count);
    }

  if (def == 1) free(dest_file);
  
  return dest_list;
}

void empty_dest_list(dest_t * dest_list)
{
  int i;

  for (i = 0; i < dest_list->count; ++i)
    {
      free(*(dest_list->dests + i + NUM_CHECK));
      free(*(dest_list->destp + i + NUM_CHECK));
    }
  free(dest_list->dests);
  dest_list->dests = NULL;
  free(dest_list->destp);
  dest_list->destp = NULL;
  dest_list->count = 0;

  return;
}

void empty_dest_list_on_close(int status, void * arg)
{
  empty_dest_list((dest_t *) arg);

  return;
}


int write_dests_to_file(const char *filename, const dest_t *dest_list)
{
  int i;
  FILE *fp = fopen(filename, "w");

  for (i = 0; i < dest_list->count; ++i)
    {
      fprintf(fp, "%s\n", *(dest_list->destp + i));
    }

  fclose(fp);
  
  return 0;
}
