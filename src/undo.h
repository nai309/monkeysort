#ifndef UNDO_H
#define UNDO_H
#include "defs.h"
#include "file_parsing.h"

enum undo_types {
  NONE,
  MOVE,
  COPY,
  MULTI_START,
  MULTI_STOP
};

typedef struct op_data {
  char * path;
  file_entry * file;
  char * dest;
  char * newname;
  time_t mtime;
} op_data;

typedef struct undo_node {
  int operation;
  op_data * data;
  struct undo_node * next;
  struct undo_node * prev;
} undo_node;

typedef struct undo_list {
  int total;
  undo_node *tail;
  undo_node *head;
} undo_list_t;

#define undo_mstart() undo_push(MULTI_START, NULL, NULL, NULL, NULL)
#define undo_mstop() undo_push(MULTI_STOP, NULL, NULL, NULL, NULL)

int undo_init();
int undo_free_node(undo_node * node);
op_data * fill_data(char * path, file_entry * file, char * dest, char * newname);
int undo_eat_head(undo_list_t * stack);
int undo_eat_tail(undo_list_t * stack);
int undo_append(undo_list_t * stack, int undo_type, op_data * data);
void undo_push_r(undo_list_t * stack, int undo_type, char * path, file_entry * file, char * dest, char * newname);
void undo_push(int undo_type, char * path, file_entry * file, char * dest, char * newname);
int undo_pop_r(undo_list_t * stack);
void undo_pop();
int pop_move(undo_list_t * stack);
int pop_copy(undo_list_t * stack);
int pop_multi(undo_list_t * stack);
int pop_clean(undo_list_t * stack);
void undo_empty_list(undo_list_t * stack);
void undo_empty_list_on_close();
  
#endif //UNDO_H
