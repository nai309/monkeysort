#ifndef MISC_H
#define MISC_H
#include "defs.h"
#include "dests.h"


void list_dests(dest_t *list);
void dest_line(dest_t *list);
void print_count(int status, void * arg);
//void help_msg();

#endif //MISC_H
