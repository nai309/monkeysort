#include "defs.h"
#include "vect.h"

#define BUFFER_INC 4

vect_list_t vect_list_init(int (*decon)(void * item))
{
  vect_list_t vect_list = {0, 0, 0, decon, NULL};
  
  return vect_list;
}

//must be free'd outside of function
vect_list_t * vect_list_init_m(int (*decon)(void * item))
{
  vect_list_t * vect_list = malloc(1 * sizeof(vect_list_t));

  *vect_list = vect_list_init(decon);
  
  return vect_list;
}

int vect_list_append(vect_list_t * vect_list, void * item)
{
  if (vect_list->count + 1 > vect_list->buffer)
    {
      vect_list->buffer += BUFFER_INC;
      vect_list->list = realloc(vect_list->list, (vect_list->buffer * sizeof(void *)));
      //initializes new list spaces to NULL
      for (int i = vect_list->count; i < vect_list->buffer; ++i)
	{
	  vect_list_item(vect_list, i) = NULL;
	}
    }

  (vect_list->count)++;
  *((vect_list->list) + (vect_list->count - 1)) = item;
  
  return(0);
}

int vect_list_remove(vect_list_t * vect_list, int num, int dist)
{
  int i;

  if (dist == 0)
    {
      return 0;
    }
  else if (dist < 0)
    {
      printf("distance less than zero\n");
      return 1;
    }
  else if ((num + dist) > vect_list->count) //may not catch one position overflow
    {
      printf("attempt to remove past list end\n");
      return 1;
    }

  for (i = num; i < (num + dist); ++i)
    {
      vect_list->decon(vect_list_item(vect_list, i));
      vect_list_item(vect_list, i) = NULL;
    }

  if ((num + dist) < (vect_list->count - 1))
    {
      void * hold = NULL;
      
      for (i = num; i + dist  < vect_list->count; ++i)
	{
	  hold = vect_list_item(vect_list, i + dist);
	  vect_list_item(vect_list, i + dist) = NULL;
	  vect_list_item(vect_list, i) = hold;
	}
    }

  vect_list->count -= dist;

  if (vect_list->count > 0)
    vect_list->buffer = ((vect_list->count / BUFFER_INC) + 1) * BUFFER_INC;
  else
    vect_list->buffer = 0;

  vect_list->list = realloc(vect_list->list, (vect_list->buffer * sizeof(void *)));

  return(0);
}

//vect_list_insert

int vect_list_index(vect_list_t * vect_list, int direction)
{
  if ((vect_list->current + direction) < 0 ||
      (vect_list->current + direction) >= vect_list->count)
    {
      return(1);
    }

  vect_list->current += direction;

  return(0);
}

int vect_list_decon(void * item)
{
  int hold = 0;
  vect_list_t * vect_list = (vect_list_t *) item;

  hold = vect_list_empty(vect_list);

  free(vect_list);
  
  return hold;
}

int anon_list_decon(void * item)
{
  int hold = 0;
  anon_container * anon = (anon_container *) item;

  anon->decon(anon->item);

  free(anon);
  
  return hold;
}

void remove_vect_list_on_close(int status, void * vect_list_v)
{
  vect_list_decon(vect_list_v);

  return;
}
