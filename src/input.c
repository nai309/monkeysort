#include "defs.h"
#include "input.h"
#include "img.h"
#include "file_parsing.h"
#include "dest_window.h"

point_t mouse_delta = {0,0}, old_delta = {0,0};

void get_input(vect_list_t * file_vect_list, vect_list_t *dest_vect_list)
{
  XEvent event;
  KeySym current_key;
  mod_t mod = MOD_NONE;
  uint i;
  input_arg_list_t arg_list;

  static point_t mouse_pos = {0,0}, hold_pos = {0,0};
  static int drag_state = 0;

  //arg_list.file_list = file_list_current(file_vect_list);
  //arg_list.dest_list = dest_list_current(dest_vect_list);
  arg_list.file_vect_list = file_vect_list;
  arg_list.dest_vect_list = dest_vect_list;
  
  do
    {
      XNextEvent(display.disp, &event);
      switch (event.type)
	{
	  
	case Expose:
	  /*display.updates = imlib_update_append_rect(display.updates,
						     event.xexpose.x,
						     event.xexpose.y,
						     event.xexpose.width,
						     event.xexpose.height);*/
	  update_render = 1;
	  break;

	  //mouse and mousewheel events
	case ButtonPress:
	  if (event.xbutton.button == Button1)
	    {
	      hold_pos = mouse_pos;
	      drag_state = 1;
	    }
	  else if (event.xbutton.button == Button4)
	    {
	      mod = convert_mod(event.xkey.state);
	      if (mod == MOD_SHIFT)
		{
		  zoom_image(1);
		}
	      else
		{
		  cycle_cont(arg_list, UP);
		}
	    }
	  else if (event.xbutton.button == Button5)
	    {
	      mod = convert_mod(event.xkey.state);
	      if (mod == MOD_SHIFT)
		{
		  zoom_image(-1);
		}
	      else
		{
		  cycle_cont(arg_list, DOWN);
		}
	    }
	  break;

	case ButtonRelease:
	  if (event.xbutton.button == Button1)
	    {
	      old_delta = mouse_delta;
	      drag_state = 0;
	    }
	  break;

	case MotionNotify:
	  mouse_pos.x = event.xmotion.x;
	  mouse_pos.y = event.xmotion.y;
	  
	  break;
	  
	case KeymapNotify:
	  XRefreshKeyboardMapping(&event.xmapping);
	  break;
	  
	  //use fallthrough to include mouse events  
	case KeyPress: //include repeat key protection
	  mod = convert_mod(event.xkey.state);
	  XLookupString(&event.xkey, NULL, 0, &current_key, NULL);

	  for ( i=0; i < control_size; ++i )
	    {
	      if (mod == controls[i].mod /*|| !(mod)*/)
		{
		  if (current_key == controls[i].key)
		    {
		      controls[i].action(arg_list, controls[i].ext);
		      break;
		    }
		}
	    }
	  dest_cont(arg_list, current_key);
	  
	  break;
	}
    }
  while (XPending(display.disp));

  if (drag_state == 1)
    {
      mouse_delta.x = mouse_pos.x - hold_pos.x + old_delta.x;
      mouse_delta.y = mouse_pos.y - hold_pos.y + old_delta.y;
      update_render = 1;
    }
  
  if (update_render == 1)
    {
      loading_img(img_window.path);
      if (dest_window_state == 1)
	{
	  dest_window_redrw((dest_list_current(dest_vect_list)));
	}
      render_img();
      update_render = 0;
    }
  return;
}
