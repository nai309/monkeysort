#ifndef FILE_IO_H
#define FILE_IO_H
#include "defs.h"

int get_file_len(char * filename);
char * read_file_line(FILE *filep);
char **str_list_from_file(char *filename, int *len);

#endif //FILE_IO_H
