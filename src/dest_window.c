#include "defs.h"
#include "dest_window.h"

extern unsigned int dest_keys[DESTMAX];

int dest_window_state = 1;

int dest_window_redrw(dest_t * dests)
{
  int vertical = 0, height = 0;
  int i, nonum = NUM_CHECK, line_len = 0;
  char * text_line = NULL;
  
  imlib_context_set_font(display.font);
  imlib_context_set_image(img_window.img_buffer);
  imlib_context_set_blend(1);

  for (i = nonum; i < (dests->count + nonum); ++i)
	{
	  line_len = snprintf(NULL, 0, "%i - %s - %s",
			      ((i + 1) - nonum),
			      XKeysymToString(dest_keys[i]),
			      *(dests->destp + i));
	  text_line = realloc(text_line, ((line_len + 1) * sizeof(char)));
	  sprintf((text_line), "%i - %s - %s",
		  ((i + 1) - nonum),
		  XKeysymToString(dest_keys[i]),
		  *(dests->destp + i));
	  imlib_context_set_color(0, 0, 0, 255);
	  imlib_text_draw_with_return_metrics(2, height + 1, text_line, NULL, NULL, NULL, NULL);
	  imlib_context_set_color(255, 255, 255, 255);
	  imlib_text_draw_with_return_metrics(1, height, text_line, NULL, NULL, NULL, &vertical);
	  height += vertical + (vertical / 10);
	}
  
  imlib_context_set_blend(0);
  
  free(text_line);
  
  return(0);
}

int dest_window_toggle()
{  
  if (dest_window_state == 0)
    {
      dest_window_state = 1;
      update_render = 1;
    }
  else if (dest_window_state == 1)
    {
      dest_window_state = 0;
      update_render = 1;
    }
  
  return(0);
}
