#include "defs.h"
#include <sys/stat.h>
#include "file_io.h"

#define INC 4

//gets the number of non-empty lines in a text file
int get_file_len(char * filename)
{
  int len = 0, n = 0;
  char buff_char = '\0';
  FILE * filep = fopen(filename, "r");
  
  buff_char = getc(filep);
  
  while(!feof(filep))
    {
      buff_char = getc(filep);
      if (buff_char == '\n')
	{
	  if (n != 1)
	    {
	      len++;
	      n = 1;
	    }
	}
      else
	n = 0;
    }
  fseek(filep, -1, SEEK_CUR);
  buff_char = getc(filep);
  if (buff_char != '\n')
    len++;

  fclose(filep);
    
  return len;
}

//ignores extra blank lines
char * read_file_line(FILE *filep)
{
  int increment = INC, buffer_size = 4;
  int i = 0;
  char *buffer = NULL, buff_char;

  buff_char = fgetc(filep);
  while (buff_char == '\n')
    buff_char = fgetc(filep);
  
  if (feof(filep)) { return NULL; }

  buffer = malloc(sizeof(char) * increment);
  
  *buffer = buff_char;
  increment--;
  i++;
  
  buff_char = fgetc(filep);
  while (buff_char != '\n')
    {
      if (increment > 0)
	{
	  *(buffer + i) = buff_char;
	  increment--;
	}
      else
	{
	  buffer = realloc(buffer, sizeof(char) * (buffer_size + INC));
	  increment = INC - 1;
	  buffer_size += INC;
	  *(buffer + i) = buff_char;
	}
      i++;
      buff_char = fgetc(filep);
      if (feof(filep))
	break;
    }
  
  if (increment <= 0)
    buffer = realloc(buffer, sizeof(char) * (buffer_size + 1));
  *(buffer + i) = '\0';
  
  return buffer;  
}

//creates an array of strings from a file, one string for line in the file
char **str_list_from_file(char *filename, int *len)
{
  int i, list_len = 1;
  FILE *fp = fopen(filename, "r");
  char **str_list = NULL;
  struct stat file_stat;

  stat(filename, &file_stat);
  if (S_ISDIR(file_stat.st_mode))
    {
      printf("file: %s is a directory, exiting\n", filename);
      exit(1);
    }

  list_len = get_file_len(filename);
  str_list = calloc(list_len, sizeof(char *));
  
  for (i = 0; i < list_len; ++i)
    {
      *(str_list + i) = read_file_line(fp);
    }

  fclose(fp);

  *len = list_len;
  return str_list;
}

