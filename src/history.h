#ifndef HISTORY_H
#define HISTORY_H
#include "defs.h"

typedef struct hist_node {
  char *text;
  struct hist_node *next;
  struct hist_node *prev;
} hist_node;

typedef struct hist_list {
  int total;
  hist_node *start;
  hist_node *current;
  hist_node *recent;
  hist_node *head;
} hist_list;

extern hist_list hists[HIST_NUM];
extern hist_list short_list[1];

int hist_init();
int hist_append(hist_list *history);
hist_node *index_node(hist_node *node, int direction);
void hist_trav(int direction, char **text, hist_list *history);
void hist_set(char *str, hist_list *history);
void hist_empty_list(hist_list * history);
void hist_empty_on_close();

#endif //HISTORY_H
