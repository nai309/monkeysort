# -*- org-export-with-sub-superscripts: nil; -*-
#+OPTIONS: toc:nil
#+TITLE: MONKEYSORT

* NAME

monkeysort - Specialized image viewer designed for manual image sorting

* SYNOPSIS

*monkeysort* [OPTIONS] /SRCDIR/ [DESTDIR(1-30)]

* DESCRIPTION

*monkeysort* is a specialized image viewer designed to make manually sorting images based on content as efficient as possible.

* OPTIONS

** -h, --help

prints a help message.

** -o, --overwrite

enable overwriting files when moving to destination.

** -a, --alphabetic

sort files alphabetically.

** -u, --recent

sort by mod time from newest to oldest.

** -i /SRCDIR/, --inputdir /SRCDIR/

uses /SRCDIR/ for source directory.

** -s /ARG/, --start /ARG/

start program at /ARG/ position in the file list.

** -r, --rename

move and rename mode, when filename already exists, appends (1) to filename and moves to destination.

** -e, --extention

makes rename operation also allow editing of file extension.

** -q, --quiet

quiet mode, suppresses printing the file list position and size when the program exits.

** -v, --verbose

prints libpng errors to the terminal

** -n, --nonum

disables use of the numpad for destination selection, limits primary destinations to the top row number keys and the function keys, and the destination limit to 20.

** -d, --destfile

specify a destination list file to load destinations from.

** -l, --no-list-window

starts without the destination list window visible.

** -w /DIR/, --working-directory /DIR/

specifies the directory /DIR/ to use as the working directory for relative file movement and for directory tab completions.

** -t, --slide-up-text

embeds the text input box into the main window, rather than creating a new window.

** -c /CONFIG_FILE/

specify config file to use.
overrides default config file.

** -f /PATTERN/, --filter /PATTERN/

filters the filenames of the results based on /PATTERN/.
/PATTERN/ must be a valid shell pattern.

** --no-default-destfile

disable loading of a default destfile if one is found in the working directory.

** --generate-config /PATH/

generate config file based on all the current configuration parameters.

* CONTROLS

** UP, DOWN

cycles forward and backward in the file list, also works with *MS_WHEEL* up and down.

** KP_ENTER

cycles one file forward in the file list.

** KP_[1-0]

moves current file to key's matching destination.

** [1-0]

same as *KP_[1-0]*, but for destinations 11-20.

** F[1-10]

same as *KP_[1-0]* and *[1-0]*, but for destinations 21-30.

** ENTER

allows for entering a custom destination for the current file.

** M

multi *ENTER*, moves the current file and the (n) files following it to a custom destination.

use: enter number of files, press return, enter destination, press return.

** C-M

multi *ENTER*, but for the current file and the (n) files preceding it.

** /

scroll-selection mode.
once pressed, the user can scroll through the filelist.
the next use of the *ENTER* key or any destination key will move all files in the scroll selection area.

** A

adds new destination to end of destination list if not already at 30 destinations.

** C

changes the directory pointed to by destination (n).

use: enter number of destination, press return, enter new destination, press return.

** L

toggle destination key overlay.

** K

prints destinations to the terminal in a copyable format.

** P

prints the filename of the current file to the terminal.

** S

seek to position in file list.

** R

rename file in file list.

note: filename should not have a leading slash or path.

** N

creates a new directory with a given name, can be relative, relative directories will be relative to the path the program was run in.

** J

plays video with the set video player, if the currently selected file is a video.
Enabled only if the program was compiled with the enable-video option.

** C-D

deletes the current file in the filelist, with confirmation box.

note: *CANNOT BE UNDONE*

** C-U

undoes the last move, copy, or rename operation.

** C-X

removes the highest numbered destination.

** C-P

specify filename to print current list of destinations to.

** C-C

activate to copy the next file acted on to the destination, rather than move it.
automatically turns off after opertation.

** C-S-C

activate copy mode until manually toggled off again.

** LEFT, RIGHT

switch between available destination lists.

** S-LEFT, S-RIGHT

switch between available filelists.

** S-UP, S-DOWN, S-MS_WHEEL

zoom in or out on the current image.

** Q, ESCAPE

exits the program.

* TEXT INPUT

When activating a function that requires text input, a text box window will be created.
This text box has both a history list of prior entries, accessed with the *UP* key,
and *TAB* completion for folder names when entering a destination.

* TEXT INPUT CONTROLS

** ENTER

finishes text input.

** UP

accesses the history list, and moves back in the history list while in the list.

** DOWN

moves forward in the history list.

** TAB

performs tab completion for directories, and cycles forward through completion
candidates if there was more than one.

** S-TAB

cycles backwards through completion candidates.

** C-c

cancels text input.

* KNOWN BUGS

No known bugs.
