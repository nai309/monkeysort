.TH "MONKEYSORT" "1" 

.SH "NAME"
.PP
monkeysort - Specialized image viewer designed for manual image sorting

.SH "SYNOPSIS"
.PP
\fBmonkeysort\fP [OPTIONS] \fISRCDIR\fP [DESTDIR(1-30)]

.SH "DESCRIPTION"
.PP
\fBmonkeysort\fP is a specialized image viewer designed to make manually sorting images based on content as efficient as possible.

.SH "OPTIONS"
.SS "-h, --help"
.PP
prints a help message.

.SS "-o, --overwrite"
.PP
enable overwriting files when moving to destination.

.SS "-a, --alphabetic"
.PP
sort files alphabetically.

.SS "-u, --recent"
.PP
sort by mod time from newest to oldest.

.SS "-i \fISRCDIR\fP, --inputdir \fISRCDIR\fP"
.PP
uses \fISRCDIR\fP for source directory.

.SS "-s \fIARG\fP, --start \fIARG\fP"
.PP
start program at \fIARG\fP position in the file list.

.SS "-r, --rename"
.PP
move and rename mode, when filename already exists, appends (1) to filename and moves to destination.

.SS "-e, --extention"
.PP
makes rename operation also allow editing of file extension.

.SS "-q, --quiet"
.PP
quiet mode, suppresses printing the file list position and size when the program exits.

.SS "-v, --verbose"
.PP
prints libpng errors to the terminal

.SS "-n, --nonum"
.PP
disables use of the numpad for destination selection, limits primary destinations to the top row number keys and the function keys, and the destination limit to 20.

.SS "-d, --destfile"
.PP
specify a destination list file to load destinations from.

.SS "-l, --no-list-window"
.PP
starts without the destination list window visible.

.SS "-w \fIDIR\fP, --working-directory \fIDIR\fP"
.PP
specifies the directory \fIDIR\fP to use as the working directory for relative file movement and for directory tab completions.

.SS "-t, --slide-up-text"
.PP
embeds the text input box into the main window, rather than creating a new window.

.SS "-c \fICONFIG\d\s-2FILE\s+2\u\fP"
.PP
specify config file to use.
overrides default config file.

.SS "-f \fIPATTERN\fP, --filter \fIPATTERN\fP"
.PP
filters the filenames of the results based on \fIPATTERN\fP.
\fIPATTERN\fP must be a valid shell pattern.

.SS "--no-default-destfile"
.PP
disable loading of a default destfile if one is found in the working directory.

.SS "--generate-config \fIPATH\fP"
.PP
generate config file based on all the current configuration parameters.

.SH "CONTROLS"
.SS "UP, DOWN"
.PP
cycles forward and backward in the file list, also works with \fBMS\d\s-2WHEEL\s+2\u\fP up and down.

.SS "KP\d\s-2ENTER\s+2\u"
.PP
cycles one file forward in the file list.

.SS "KP\d\s-2[1\s+2\u-0]"
.PP
moves current file to key's matching destination.

.SS "[1-0]"
.PP
same as \fBKP\d\s-2[1\s+2\u-0]\fP, but for destinations 11-20.

.SS "F[1-10]"
.PP
same as \fBKP\d\s-2[1\s+2\u-0]\fP and \fB[1-0]\fP, but for destinations 21-30.

.SS "ENTER"
.PP
allows for entering a custom destination for the current file.

.SS "M"
.PP
multi \fBENTER\fP, moves the current file and the (n) files following it to a custom destination.

.PP
use: enter number of files, press return, enter destination, press return.

.SS "C-M"
.PP
multi \fBENTER\fP, but for the current file and the (n) files preceding it.

.SS "/"
.PP
scroll-selection mode.
once pressed, the user can scroll through the filelist.
the next use of the \fBENTER\fP key or any destination key will move all files in the scroll selection area.

.SS "A"
.PP
adds new destination to end of destination list if not already at 30 destinations.

.SS "C"
.PP
changes the directory pointed to by destination (n).

.PP
use: enter number of destination, press return, enter new destination, press return.

.SS "L"
.PP
toggle destination key overlay.

.SS "K"
.PP
prints destinations to the terminal in a copyable format.

.SS "P"
.PP
prints the filename of the current file to the terminal.

.SS "S"
.PP
seek to position in file list.

.SS "R"
.PP
rename file in file list.

.PP
note: filename should not have a leading slash or path.

.SS "N"
.PP
creates a new directory with a given name, can be relative, relative directories will be relative to the path the program was run in.

.SS "J"
.PP
plays video with the set video player, if the currently selected file is a video.
Enabled only if the program was compiled with the enable-video option.

.SS "C-D"
.PP
deletes the current file in the filelist, with confirmation box.

.PP
note: \fBCANNOT BE UNDONE\fP

.SS "C-U"
.PP
undoes the last move, copy, or rename operation.

.SS "C-X"
.PP
removes the highest numbered destination.

.SS "C-P"
.PP
specify filename to print current list of destinations to.

.SS "C-C"
.PP
activate to copy the next file acted on to the destination, rather than move it.
automatically turns off after opertation.

.SS "C-S-C"
.PP
activate copy mode until manually toggled off again.

.SS "LEFT, RIGHT"
.PP
switch between available destination lists.

.SS "S-LEFT, S-RIGHT"
.PP
switch between available filelists.

.SS "S-UP, S-DOWN, S-MS\d\s-2WHEEL\s+2\u"
.PP
zoom in or out on the current image.

.SS "Q, ESCAPE"
.PP
exits the program.

.SH "TEXT INPUT"
.PP
When activating a function that requires text input, a text box window will be created.
This text box has both a history list of prior entries, accessed with the \fBUP\fP key,
and \fBTAB\fP completion for folder names when entering a destination.

.SH "TEXT INPUT CONTROLS"
.SS "ENTER"
.PP
finishes text input.

.SS "UP"
.PP
accesses the history list, and moves back in the history list while in the list.

.SS "DOWN"
.PP
moves forward in the history list.

.SS "TAB"
.PP
performs tab completion for directories, and cycles forward through completion
candidates if there was more than one.

.SS "S-TAB"
.PP
cycles backwards through completion candidates.

.SS "C-c"
.PP
cancels text input.

.SH "KNOWN BUGS"
.PP
No known bugs.
